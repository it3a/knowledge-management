package com.it3a.yada.Services.Algorithm;

import com.google.cloud.firestore.*;
import com.it3a.yada.Components.DatabaseComponent;
import com.it3a.yada.Modals.Behavior;
import com.it3a.yada.Modals.User;
import com.it3a.yada.Services.UserService;
import com.it3a.yada.library.GeoFire.core.GeoHashQuery;
import com.it3a.yada.library.GeoFire.utils.GeoUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ExecutionException;
import java.util.stream.Collectors;

@Service
public class YadaCore {

    @Autowired
    private YadaCoreScore yadaCoreScore;
    @Autowired
    private UserService userService;

    private User currentUser;

    public Map<User, Integer> run(User currentUser) {
        this.currentUser = currentUser;
        Set<User> coreMatches = getCoreMatches();
        return yadaCoreScore.run(coreMatches, currentUser);
    }

    /**
     * Get the core users filtered by location and gender on the firestore database and by age on the backend
     * @return List of all matching core users.
     */
    private Set<User> getCoreMatches() {

        Set<User> coreUsers = new HashSet<>();

        Set<Query> queries = getQueriesForDocumentsAround();

        queries.forEach(query -> {
            try {
                // execute query
                QuerySnapshot querySnapshot = query.get().get();

                querySnapshot.getDocuments().forEach(document -> {
                    User user = document.toObject(User.class);
                    user.setId(document.getId());
                    // setting geoPoint manually
                    user.setGeoPoint(document.getGeoPoint("geoPoint"));
                    // lets check whether the age is suitable and its within the range of user preferences
                    if (isAgeSuitable(user) && isWithinRange(currentUser, user) && userBehaviorValidation(currentUser, user)) {
                        coreUsers.add(user);
                    }
                });
            } catch (InterruptedException | ExecutionException e) {
                e.printStackTrace();
            }
        });


        return coreUsers;
    }

    /**
     * Check if the age is suitable for the current user
     * @param userToCheck   the user to check
     * @return  is suitable?
     */
    private boolean isAgeSuitable(User userToCheck) {
        int userToCheckAge = userToCheck.getAge();
        int suitableAgeFrom = currentUser.getMatchAge().getLookingFrom();
        int suitableAgeTo = currentUser.getMatchAge().getLookingTo();
        // if user matches returns true
        return userToCheckAge >= suitableAgeFrom && userToCheckAge <= suitableAgeTo;
    }

    // compares two users and checks the distance between their coordinates.
    private boolean isWithinRange(User currentUser, User userToCheck) {
        return GeoUtils.distance(currentUser.getGeoPoint(), userToCheck.getGeoPoint()) <= GeoUtils.kmToMeters(currentUser.getSearchDistance());
    }

    // querying the database to get documents around current user location and filtering
    // by user gender preferences
    private Set<Query> getQueriesForDocumentsAround() {
        Firestore db = DatabaseComponent.getDatabase();

        CollectionReference usersCollection = db.collection("users");

        Set<GeoHashQuery> queriesAtLocation = GeoHashQuery.queriesAtLocation(currentUser.getGeoPoint(), GeoUtils.kmToMeters(currentUser.getSearchDistance()));

        return queriesAtLocation.stream()
                .map(geoHashQuery -> usersCollection
                        .whereGreaterThan("geoHash", geoHashQuery.getStartValue())
                        .whereLessThan("geoHash", geoHashQuery.getEndValue())
                        .whereEqualTo("genderInterest", currentUser.getGender())
                        .whereEqualTo("gender", currentUser.getGenderInterest()))
                .collect(Collectors.toSet());
    }

    // we check users behavior and determine if we suggest this user for the first time.
    // if current user already clicked like/dislike (did any behavior) we do not show it agian
    private boolean userBehaviorValidation(User currentUser, User userToCheck) {
        if(currentUser.getId().equals(userToCheck.getId())) {
            // Eliminating possibility to match yourself
            return false;
        }

        CollectionReference userBehaviors = userService.getUserDocumentById(currentUser.getId()).collection("behaviors");
        try {
            List<QueryDocumentSnapshot> queryDocuments = userBehaviors.get().get().getDocuments();
            for(QueryDocumentSnapshot queryDocument : queryDocuments) {
                Behavior currentUserBehavior = queryDocument.toObject(Behavior.class);

                if(currentUserBehavior.getInterestedUserId().equals(userToCheck.getId())) {
                    return false;
                }
            }

        } catch (InterruptedException | ExecutionException e) {
            return false;
        }
        return true;
    }
}
