package com.it3a.yada.Modals;

import java.util.List;

public class ScoreItems {
    private List<Long> likes;
    private List<Long> events;
    private List<Long> languages;
    private List<Long> matches;
    private List<Long> sports;

    public ScoreItems() { }

    public ScoreItems(List<Long> likes, List<Long> events, List<Long> languages, List<Long> matches, List<Long> sports) {
        this.likes = likes;
        this.events = events;
        this.languages = languages;
        this.matches = matches;
        this.sports = sports;
    }

    public List<Long> getLikes() {
        return likes;
    }

    public void setLikes(List<Long> likes) {
        this.likes = likes;
    }

    public List<Long> getEvents() {
        return events;
    }

    public void setEvents(List<Long> events) {
        this.events = events;
    }

    public List<Long> getLanguages() {
        return languages;
    }

    public void setLanguages(List<Long> languages) {
        this.languages = languages;
    }

    public List<Long> getMatches() {
        return matches;
    }

    public void setMatches(List<Long> matches) {
        this.matches = matches;
    }

    public List<Long> getSports() {
        return sports;
    }

    public void setSports(List<Long> sports) {
        this.sports = sports;
    }
}
