package com.it3a.yada.library;

import com.it3a.yada.Modals.Message;

import java.util.Comparator;

public class SortByMessageTime implements Comparator<Message> {
    // Used for sorting messages by time
    @Override
    public int compare(Message o1, Message o2) {
        return o1.getTime().compareTo(o2.getTime());
    }
}
