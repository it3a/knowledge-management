package com.it3a.yada.Controllers;

import com.it3a.yada.Controllers.DTOs.UserProfileDTO;
import com.it3a.yada.Modals.User;
import com.it3a.yada.Services.FacebookService;
import com.it3a.yada.Services.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping(path="/user")
public class UserController {

    private UserService userService;
    private FacebookService facebookService;

    @Autowired
    public UserController(UserService userService, FacebookService facebookService) {
        this.userService = userService;
        this.facebookService = facebookService;
    }

    @PostMapping(path = "/get")
    public @ResponseBody User getUserById(@RequestParam String userId) {
        return userService.getUserById(userId);
    }

    @PostMapping(path = "/authenticate")
    public @ResponseBody User createUser(@RequestParam Long fbId, @RequestParam String fbToken) {
        return facebookService.authenticate(fbId, fbToken);
    }

    @PostMapping(path = "/update")
    public @ResponseBody User updateUser(@RequestBody UserProfileDTO userProfileDTO) {
        return userService.updateUser(userProfileDTO);
    }

    @PostMapping(path = "/update-location")
    public boolean updateUserLocation(@RequestParam double lat, @RequestParam double lng, @RequestParam String userId, @RequestParam boolean isLocationByGPS) {
        return userService.updateUserLocation(lat, lng, userId, isLocationByGPS);
    }

    @PostMapping(path = "/make-profile-picture")
    @ResponseBody
    public String setProfilePicture(@RequestParam String userId, @RequestParam String imgPath) {
        return userService.setProfilePicture(userId, imgPath);
    }

    @PostMapping(path = "/get-profile-picture")
    @ResponseBody
    public String getProfilePicture(@RequestParam String userId) {
        return userService.getProfilePicture(userId);
    }

    @PostMapping(path = "/get-pictures")
    @ResponseBody
    public List<String> getUserPictures(@RequestParam String userId) {
        return userService.getPictures(userId);
    }

    @PostMapping(path = "/upload-picture")
    @ResponseBody
    public String uploadPicture(@RequestParam String userId, @RequestParam String imgPath) {
        return userService.uploadPicture(userId, imgPath);
    }

    @PostMapping(path = "/delete-picture")
    @ResponseBody
    public List<String> deletePicture(@RequestParam String userId, @RequestParam String imgPath) {
        return userService.deletePicture(userId, imgPath);
    }
}
