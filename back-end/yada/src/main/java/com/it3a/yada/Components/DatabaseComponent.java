package com.it3a.yada.Components;

import com.google.auth.oauth2.GoogleCredentials;
import com.google.cloud.firestore.Firestore;
import com.google.cloud.firestore.FirestoreOptions;
import org.springframework.stereotype.Component;
import org.springframework.util.ResourceUtils;

import java.io.*;

@Component
public class DatabaseComponent {

    private static Firestore database;

    public DatabaseComponent() {
        GoogleCredentials credentials = null;

        try {
            InputStream serviceAccount = ResourceUtils.getURL("classpath:firebaseKey.json").openStream();
            credentials = GoogleCredentials.fromStream(serviceAccount);
        }
        catch (FileNotFoundException ex) {
            System.out.println("Exception found: " + ex.getMessage());
            System.out.println("Exception StackTrace: " + ex.getStackTrace().toString());
        }
        catch (IOException ex) {
            System.out.println("Exception found: " + ex.getMessage());
            System.out.println("Exception StackTrace: " + ex.getStackTrace().toString());
        }

        FirestoreOptions options =
                FirestoreOptions.newBuilder().setCredentials(credentials).setTimestampsInSnapshotsEnabled(true).build();
        database = options.getService();
    }

    public static Firestore getDatabase() {
        return database;
    }
}
