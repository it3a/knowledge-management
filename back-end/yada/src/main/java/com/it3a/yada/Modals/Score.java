package com.it3a.yada.Modals;

import com.it3a.yada.GLOBAL;

public class Score {

    private int like;
    private int event;
    private int language;
    private int sport;



    public Score() {
        this.like = GLOBAL.DEFAULT_LIKE_SCORE;
        this.event = GLOBAL.DEFAULT_EVENT_SCORE;
        this.language = GLOBAL.DEFAULT_LANGUAGE_SCORE;
        this.sport = GLOBAL.DEFAULT_SPORT_SCORE;
    }

    public int getLike() {
        return like;
    }

    public void setLike(int like) {
        this.like = like;
    }

    public int getEvent() {
        return event;
    }

    public void setEvent(int event) {
        this.event = event;
    }

    public int getLanguage() {
        return language;
    }

    public void setLanguage(int language) {
        this.language = language;
    }

    public int getSport() {
        return sport;
    }

    public void setSport(int sport) {
        this.sport = sport;
    }
}
