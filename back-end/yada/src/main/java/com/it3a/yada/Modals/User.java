package com.it3a.yada.Modals;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.google.cloud.firestore.GeoPoint;
import com.google.cloud.firestore.annotation.Exclude;
import com.it3a.yada.Controllers.DTOs.Facebook.FbUserDTO;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoUnit;
import java.util.List;

public class User {
    private String id; // documentId
    private MatchAge matchAge;
    private String description;
    private String gender;
    private String genderInterest;
    private String geoHash;
    @JsonIgnore // we are serializing it manually. It doesn't have default constructor.
    private GeoPoint geoPoint;
    private String name;
    private List<String> pictures;
    private List<Behavior> behaviors;
    private String profilePicture;
    private Score score;
    private ScoreItems scoreItems;
    private int searchDistance;
    private int age;
    private int popularity;
    private boolean isLocationByGPS;
    // facebook
    private Long fbId;
    private String fbToken;
    private String fbEmail;

    public User() {
    }

    // constructor to parse facebook DTO to user object
    public User(FbUserDTO fbUserDTO) {
        this.fbId = fbUserDTO.getId();
        this.fbToken = fbUserDTO.getFbToken();
        this.fbEmail = fbUserDTO.getEmail();
        this.gender = fbUserDTO.getGender();
        this.genderInterest = fbUserDTO.getGender_interest();
        this.name = fbUserDTO.getFirst_name() + " " + fbUserDTO.getLast_name();
        this.age = getFacebookAge(fbUserDTO.getBirthday());
    }

    public User(String name, String gender, String genderInterest, int age) {
        this.name = name;
        this.gender = gender;
        this.genderInterest = genderInterest;
        this.age = age;
    }

    public int getPopularity() {
        return popularity;
    }

    public void setPopularity(int popularity) {
        this.popularity = popularity;
    }

    @Exclude
    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public MatchAge getMatchAge() {
        return matchAge;
    }

    public void setMatchAge(MatchAge matchAge) {
        this.matchAge = matchAge;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getGenderInterest() {
        return genderInterest;
    }

    public void setGenderInterest(String genderInterest) {
        this.genderInterest = genderInterest;
    }

    public String getGeoHash() {
        return geoHash;
    }

    public void setGeoHash(String geoHash) {
        this.geoHash = geoHash;
    }

    public GeoPoint getGeoPoint() {
        return geoPoint;
    }

    public void setGeoPoint(GeoPoint geoPoint) {
        this.geoPoint = geoPoint;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<String> getPictures() {
        return pictures;
    }

    public void setPictures(List<String> pictures) {
        this.pictures = pictures;
    }

    public String getProfilePicture() {
        return profilePicture;
    }

    public void setProfilePicture(String profilePicture) {
        this.profilePicture = profilePicture;
    }

    public Score getScore() {
        return score;
    }

    public void setScore(Score score) {
        this.score = score;
    }

    public ScoreItems getScoreItems() {
        return scoreItems;
    }

    public void setScoreItems(ScoreItems scoreItems) {
        this.scoreItems = scoreItems;
    }

    public int getSearchDistance() {
        return searchDistance;
    }

    public void setSearchDistance(int searchDistance) {
        this.searchDistance = searchDistance;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public List<Behavior> getBehaviors() {
        return behaviors;
    }

    public void setBehaviors(List<Behavior> behaviors) {
        this.behaviors = behaviors;
    }

    public boolean getIsLocationByGPS() {
        return isLocationByGPS;
    }

    public void setIsLocationByGPS(boolean locationByGPS) {
        isLocationByGPS = locationByGPS;
    }

    public Long getFbId() {
        return fbId;
    }

    public void setFbId(Long fbId) {
        this.fbId = fbId;
    }

    public String getFbToken() {
        return fbToken;
    }

    public void setFbToken(String fbToken) {
        this.fbToken = fbToken;
    }

    public String getFbEmail() {
        return fbEmail;
    }

    public void setFbEmail(String fbEmail) {
        this.fbEmail = fbEmail;
    }

    // converts facebook birthday string to users age
    private int getFacebookAge(String date) {
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("MM/d/yyyy");
        //convert String to LocalDate
        LocalDate localDate = LocalDate.parse(date, formatter);
        return (int) ChronoUnit.YEARS.between(localDate, LocalDate.now());
    }
}
