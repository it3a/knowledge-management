package com.it3a.yada.Services;

import com.google.api.core.ApiFuture;
import com.google.cloud.Timestamp;
import com.google.cloud.firestore.*;
import com.it3a.yada.Components.DatabaseComponent;
import com.it3a.yada.Controllers.DTOs.MatchedUserDTO;
import com.it3a.yada.Modals.Behavior;
import com.it3a.yada.Modals.Message;
import com.it3a.yada.Modals.User;
import com.it3a.yada.library.SortByMessageTime;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.ExecutionException;
import java.util.stream.Collectors;

@Service
public class MessagingService {

    public Message sendMessage(String senderId, String receiverId, String message) {
        Message chat = new Message();
        chat.setReceiverId(receiverId);
        chat.setSenderId(senderId);
        chat.setMessage(message);
        chat.setTime(Timestamp.now());

        Firestore database = DatabaseComponent.getDatabase();
        CollectionReference messages = database.collection("messages");
        messages.add(chat);
        return chat;
    }

    public List<Message> getMessagesChat(String user1, String user2) {

        Firestore database = DatabaseComponent.getDatabase();
        CollectionReference messagesRef = database.collection("messages");

        //due to firestore limitation (they don't have OR operator) we need two queries - to get sender messages
        // and to get receiver messages. More info https://firebase.google.com/docs/firestore/query-data/queries

        Query senderMessagesQuery =  messagesRef
               .whereEqualTo("receiverId", user1)
               .whereEqualTo("senderId", user2)
                .orderBy("time", Query.Direction.DESCENDING);

        ApiFuture<QuerySnapshot> senderMessagesFuture = senderMessagesQuery.get();

        Query receiverMessagesQuery =  messagesRef
                .whereEqualTo("receiverId", user2)
                .whereEqualTo("senderId", user1)
                .orderBy("time", Query.Direction.DESCENDING);

        ApiFuture<QuerySnapshot> receiverMessagesFuture = receiverMessagesQuery.get();

        try {
            // merge two collections in one
            List<Message> messages = new ArrayList<>(mapMessagesToList(senderMessagesFuture.get().getDocuments()));
            List<Message> receiverMessages = mapMessagesToList(receiverMessagesFuture.get().getDocuments());
            messages.addAll(receiverMessages);
            // sort them by time
            messages.sort(new SortByMessageTime());
            return messages;
        } catch (InterruptedException | ExecutionException e) {
            e.printStackTrace();
        }
        // failed to query. Lets return an empty list
        return new ArrayList<>();
    }

    private List<Message> mapMessagesToList(List<QueryDocumentSnapshot> queryDocumentSnapshots) {
         return queryDocumentSnapshots.stream()
                .map(document -> {
                    Message message = document.toObject(Message.class);
                    message.setId(document.getId());
                    return message;
                }).collect(Collectors.toList());
    }
}
