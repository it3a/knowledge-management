package com.it3a.yada.Modals;

public class MatchAge {
    private int lookingFrom;
    private int lookingTo;

    public MatchAge() {
    }

    public MatchAge(int lookingFrom, int lookingTo) {
        this.lookingFrom = lookingFrom;
        this.lookingTo = lookingTo;
    }

    public int getLookingFrom() {
        return lookingFrom;
    }

    public void setLookingFrom(int lookingFrom) {
        this.lookingFrom = lookingFrom;
    }

    public int getLookingTo() {
        return lookingTo;
    }

    public void setLookingTo(int lookingTo) {
        this.lookingTo = lookingTo;
    }
}
