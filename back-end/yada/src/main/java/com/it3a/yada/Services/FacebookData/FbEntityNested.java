package com.it3a.yada.Services.FacebookData;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class FbEntityNested {
    private List<Map> data;
    private Map<Object, Object> paging;

    public FbEntityNested() {
    }

    private List<Long> getIdsFromData(List<Map> currentEvents) {
      return currentEvents.stream()
              .map(event -> Long.valueOf(event.get("id").toString()))
              .collect(Collectors.toList());
    }

    public List<Map> getData() {
        return data;
    }

    public void setData(List<Map> data) {
        this.data = data;
    }

    public Map<Object, Object> getPaging() {
        return paging;
    }

    public void setPaging(Map<Object, Object> paging) {
        this.paging = paging;
    }

    public List<Long> getIds() {
        // TODO: call eventsToIds with every event from paging
        return getIdsFromData(data);
    }
}
