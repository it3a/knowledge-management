package com.it3a.yada.Services;

import com.google.api.core.ApiFuture;
import com.google.cloud.firestore.*;
import com.it3a.yada.Components.DatabaseComponent;
import com.it3a.yada.Controllers.DTOs.MatchedUserDTO;
import com.it3a.yada.GLOBAL;
import com.it3a.yada.Modals.Behavior;
import com.it3a.yada.Modals.Score;
import com.it3a.yada.Modals.ScoreItems;
import com.it3a.yada.Modals.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutionException;
import java.util.stream.Collectors;

@Service
public class BehaviorService {

    @Autowired
    private UserService userService;

    public Behavior createBehavior(String userId, String chosenUserId, boolean isLiked){
        Behavior behavior = new Behavior(chosenUserId, isLiked);
        Firestore database = DatabaseComponent.getDatabase();
        database.collection("users").document(userId).collection("behaviors").add(behavior);
        User user = userService.getUserById(userId);
        User chosenUser = adjustPopularity(chosenUserId, isLiked);
        adjustUserScore(user, chosenUser, isLiked);

        return behavior;
    }

    private User adjustPopularity(String chosenUserId, boolean isLiked){

        User chosenUser = userService.getUserById(chosenUserId);

        int popularity = chosenUser.getPopularity();

        if (isLiked) {
            popularity += GLOBAL.DEFAULT_USER_BEHAVIOR_CHANGE;
        } else {
            popularity -= GLOBAL.DEFAULT_USER_BEHAVIOR_CHANGE;
        }

        Firestore database = DatabaseComponent.getDatabase();
        DocumentReference userDocRef = database.collection("users").document(chosenUserId);
        ApiFuture<WriteResult> adjustPopularity = userDocRef.update("popularity", popularity);

        try {
            adjustPopularity.get();
        } catch (InterruptedException | ExecutionException e) {
            e.printStackTrace();
        }

        return userService.getUserById(chosenUserId);
    }

    // this method will adjust user preferences (will teach the system what user likes (or not)).
    // For example, if user liked chosen person who had common languages, users language preference will increase
    private void adjustUserScore(User user, User chosenUser, boolean isLiked) {
        ScoreItems userScoreItems = user.getScoreItems();
        ScoreItems chosenUserScoreItems = chosenUser.getScoreItems();

        Score userScore = user.getScore();

        // lets check if both users registered by facebook and we were able to retrieve their data
        if(userScoreItems != null && chosenUserScoreItems != null) {
            // getting the number of things two users have in common
            int sportsInCommon = findSameIds(userScoreItems.getSports(), chosenUserScoreItems.getSports());
            int langInCommon = findSameIds(userScoreItems.getLanguages(), chosenUserScoreItems.getLanguages());
            int eventsInCommon = findSameIds(userScoreItems.getEvents(), chosenUserScoreItems.getEvents());
            int likesInCommon = findSameIds(userScoreItems.getLikes(), chosenUserScoreItems.getLikes());

            // getting existing user score (preferences)
            int sportScore = userScore.getSport();
            int langScore = userScore.getLanguage();
            int eventScore = userScore.getEvent();
            int likesScore = userScore.getLike();

            // depending whether the user was liked or not, we adjust users score (preferences)
            if (isLiked) {
                userScore.setSport(sportScore + sportsInCommon * GLOBAL.DEFAULT_USER_BEHAVIOR_CHANGE);
                userScore.setLanguage(langScore + langInCommon * GLOBAL.DEFAULT_USER_BEHAVIOR_CHANGE);
                userScore.setEvent(eventScore + eventsInCommon * GLOBAL.DEFAULT_USER_BEHAVIOR_CHANGE);
                userScore.setLike(likesScore + likesInCommon * GLOBAL.DEFAULT_USER_BEHAVIOR_CHANGE);
            } else {
                userScore.setSport(sportScore - sportsInCommon * GLOBAL.DEFAULT_USER_BEHAVIOR_CHANGE);
                userScore.setLanguage(langScore - langInCommon * GLOBAL.DEFAULT_USER_BEHAVIOR_CHANGE);
                userScore.setEvent(eventScore - eventsInCommon * GLOBAL.DEFAULT_USER_BEHAVIOR_CHANGE);
                userScore.setLike(likesScore - likesInCommon * GLOBAL.DEFAULT_USER_BEHAVIOR_CHANGE);
            }

            Firestore database = DatabaseComponent.getDatabase();
            DocumentReference userDocRef = database.collection("users").document(user.getId());
            ApiFuture<WriteResult> adjustScore = userDocRef.update("score", userScore);

            try {
                adjustScore.get();
            } catch (InterruptedException | ExecutionException e) {
                e.printStackTrace();
            }
        }

    }

    private int findSameIds(List<Long> listA, List<Long> listB){
        List<Long> common = new ArrayList<>(listA);
        common.retainAll(listB);
        return common.size();
    }

    private List<Behavior> getLikedUsersBehaviors(String userId) {
        Firestore database = DatabaseComponent.getDatabase();
        CollectionReference userBehaviors = database.collection("users").document(userId).collection("behaviors");


        Query matchedUsersQuery =  userBehaviors.whereEqualTo("liked", true);
        // Getting the filtered documents
        ApiFuture<QuerySnapshot> filteredBehaviors = matchedUsersQuery.get();

        try {
            return filteredBehaviors.get().getDocuments()
                    .stream()
                    .map(filteredBehavior -> filteredBehavior.toObject(Behavior.class))
                    .collect(Collectors.toList());
        } catch (InterruptedException | ExecutionException e) {
            e.printStackTrace();
        }
        // failed to query
        return new ArrayList<>();
    }

    // method gives a list of matched users by checking given users behavior.
    // if user liked a person, and the person liked him too - its a match.
    public List<MatchedUserDTO> getMatchedUsers(String userId) {
        List<MatchedUserDTO> matchedUsers = new ArrayList<>();
        // getting liked users behaviors
        List<Behavior> likedUsersBehaviors = getLikedUsersBehaviors(userId);
        likedUsersBehaviors.forEach(likedUsersBehavior -> {
            String likedUserId = likedUsersBehavior.getInterestedUserId();
            // lets check if liked user liked me back
            getLikedUsersBehaviors(likedUserId).forEach(likedUserBehavior -> {
                if (likedUserBehavior.getLiked() && likedUserBehavior.getInterestedUserId().equals(userId)) {
                    // its a match
                    User likedUser = userService.getUserById(likedUserId);
                    matchedUsers.add(handleMatchedUser(likedUser));
                }
            });
        });

        return matchedUsers;
    }

    // getting necessary information from the user object
    private MatchedUserDTO handleMatchedUser(User user) {
        MatchedUserDTO dto = new MatchedUserDTO();
        dto.setUserId(user.getId());
        dto.setName(user.getName());
        dto.setGender(user.getGender());
        dto.setAge(user.getAge());
        dto.setDescription(user.getDescription());
        return dto;
    }
}
