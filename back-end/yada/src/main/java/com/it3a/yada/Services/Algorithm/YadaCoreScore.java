package com.it3a.yada.Services.Algorithm;

import com.it3a.yada.Modals.Score;
import com.it3a.yada.Modals.ScoreItems;
import com.it3a.yada.Modals.User;
import org.springframework.stereotype.Service;

import java.util.Collections;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Set;

import static java.util.stream.Collectors.toMap;

@Service
public class YadaCoreScore {

    public Map<User, Integer> run(Set<User> coreUsers, User currentUser) {
        return getUserWithRating(coreUsers, currentUser);
    }

    /**
     * Rating and sorting the core users by the different scoreItems.
     * @param coreUsers A List of the core users
     * @return  A sorted map with Users and their matching score sorted desc.
     */
    private Map<User, Integer> getUserWithRating(Set<User> coreUsers, User currentUser) {
        Map<User, Integer> scoreUsers = new LinkedHashMap<>();

        coreUsers.forEach(suggestedUser -> {
            int matchScore = 0;

            Score userScore = currentUser.getScore();
            ScoreItems myScoreItems = currentUser.getScoreItems();

            ScoreItems suggestedScoreItems = suggestedUser.getScoreItems();
            // if user have score items lets give him some points
            if (suggestedScoreItems != null && myScoreItems != null) {

                if(suggestedScoreItems.getLikes() != null && myScoreItems.getLikes() != null)
                matchScore += suggestedScoreItems.getLikes().stream()
                        .filter(likeId -> myScoreItems.getLikes().contains(likeId))
                        .count() * userScore.getLike();

                if(suggestedScoreItems.getEvents() != null && myScoreItems.getEvents() != null)
                    matchScore += suggestedScoreItems.getEvents().stream()
                        .filter(eventId -> myScoreItems.getEvents().contains(eventId))
                        .count() * userScore.getEvent();

                if(suggestedScoreItems.getLanguages() != null && myScoreItems.getLanguages() != null)
                    matchScore += suggestedScoreItems.getLanguages().stream()
                        .filter(langId -> myScoreItems.getLanguages().contains(langId))
                        .count() * userScore.getLanguage();

                if(suggestedScoreItems.getSports() != null && myScoreItems.getSports() != null)
                    matchScore += suggestedScoreItems.getSports().stream()
                        .filter(sportId -> myScoreItems.getSports().contains(sportId))
                        .count() * userScore.getSport();

                matchScore += suggestedUser.getPopularity();
            }

            scoreUsers.put(suggestedUser, matchScore);
        });

        return scoreUsers.entrySet()
                .stream()
                .sorted(Collections.reverseOrder(Map.Entry.comparingByValue()))
                .collect(toMap(Map.Entry::getKey, Map.Entry::getValue, (e1, e2) -> e2, LinkedHashMap::new));
    }
}
