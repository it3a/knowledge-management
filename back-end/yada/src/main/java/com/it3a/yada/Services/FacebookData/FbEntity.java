package com.it3a.yada.Services.FacebookData;

public class FbEntity {

    private Long id;

    public FbEntity() {
    }

    public FbEntity(Long id) {
        this.id = id;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }
}
