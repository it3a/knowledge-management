package com.it3a.yada.Services.RestAPIs;

import com.it3a.yada.Controllers.DTOs.Facebook.FbUserDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

@Service
public class FacebookDetailsClient {
    private static final String FACEBOOK_API = "https://graph.facebook.com/v3.3/me";
    // fields which will be mapped to fbUserDTO
    private static final String FACEBOOK_FIELDS = "id,first_name,last_name,gender,birthday,email,languages,events,likes,favorite_teams";

    private final RestTemplate restTemplate;

    @Autowired
    public FacebookDetailsClient(RestTemplateBuilder restTemplateBuilder) {
        restTemplate = restTemplateBuilder.build();
    }

    public FbUserDTO getFbUserDetails(String fbToken) {
        final String uri = FACEBOOK_API + "?access_token={fbToken}&fields=" + FACEBOOK_FIELDS;

        Map<String, String> params = new HashMap<>();
        params.put("fbToken", fbToken);
        FbUserDTO fbUserDTO =  restTemplate.getForObject(uri, FbUserDTO.class, params);
        Objects.requireNonNull(fbUserDTO).setFbToken(fbToken);
        return fbUserDTO;
    }
}
