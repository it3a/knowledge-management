package com.it3a.yada.Modals;

import com.google.cloud.Timestamp;
import com.google.cloud.firestore.annotation.Exclude;


public class Message {
    private String id; // documentId
    private String senderId;
    private String receiverId;
    private String message;
    private Timestamp time;

    public Message() {
    }

    public Message(String senderId, String receiverId, String message, Timestamp time) {
        this.senderId = senderId;
        this.receiverId = receiverId;
        this.message = message;
        this.time = time;
    }

    @Exclude
    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getSenderId() {
        return senderId;
    }

    public void setSenderId(String senderId) {
        this.senderId = senderId;
    }

    public String getReceiverId() {
        return receiverId;
    }

    public void setReceiverId(String receiverId) {
        this.receiverId = receiverId;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Timestamp getTime() {
        return time;
    }

    public void setTime(Timestamp time) {
        this.time = time;
    }
}
