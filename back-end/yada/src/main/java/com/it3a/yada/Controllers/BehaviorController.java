package com.it3a.yada.Controllers;

import com.it3a.yada.Controllers.DTOs.MatchedUserDTO;
import com.it3a.yada.Modals.Behavior;
import com.it3a.yada.Services.BehaviorService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping(path="/behavior")
public class BehaviorController {

    private BehaviorService behaviorService;

    @Autowired
    public BehaviorController(BehaviorService behaviorService) {
        this.behaviorService = behaviorService;
    }

    @PostMapping(path = "/get-matches")
    public List<MatchedUserDTO> getMatchedUsers(@RequestParam String userId) {
        return behaviorService.getMatchedUsers(userId);
    }

    @PostMapping(path = "/create")
    public @ResponseBody Behavior create(@RequestParam String userId, @RequestParam String chosenUserId, @RequestParam boolean isLiked){
        return behaviorService.createBehavior(userId, chosenUserId, isLiked);
    }
}
