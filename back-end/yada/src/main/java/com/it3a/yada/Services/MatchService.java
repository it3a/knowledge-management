package com.it3a.yada.Services;

import com.it3a.yada.Modals.User;
import com.it3a.yada.Services.Algorithm.YadaCore;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Map;

@Service
public class MatchService {

    @Autowired
    private UserService userService;
    @Autowired
    private YadaCore yadaCore;

    private User currentUser;

    public User[] getUsers(String userId) {
        currentUser = userService.getUserById(userId);
        Map<User, Integer> matches = yadaCore.run(currentUser);
        User[] users = new User[matches.size()];
        return matches.keySet().toArray(users);
    }
}
