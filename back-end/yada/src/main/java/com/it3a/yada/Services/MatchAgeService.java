package com.it3a.yada.Services;

import com.it3a.yada.GLOBAL;
import com.it3a.yada.Modals.MatchAge;
import com.it3a.yada.Modals.User;
import org.springframework.stereotype.Service;

@Service
public class MatchAgeService {

    public MatchAge create(User user) {
        int lookingFrom = user.getAge() - GLOBAL.LOOKING_YOUNGER_THAN_USER;
        int lookingTo = user.getAge() + GLOBAL.LOOKING_OLDER_THAN_USER;
        return new MatchAge(lookingFrom, lookingTo);
    }
}
