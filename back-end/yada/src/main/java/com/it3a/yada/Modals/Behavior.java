package com.it3a.yada.Modals;

public class Behavior {
    private String interestedUserId;
    private boolean liked;

    public Behavior(String interestedUserId, boolean liked) {
        this.interestedUserId = interestedUserId;
        this.liked = liked;
    }

    public Behavior() {
    }

    public void setInterestedUserId(String interestedUserId) {
        this.interestedUserId = interestedUserId;
    }

    public String getInterestedUserId() {
        return interestedUserId;
    }

    public void setLiked(boolean liked) {
        this.liked = liked;
    }

    public boolean getLiked() {
        return liked;
    }
}
