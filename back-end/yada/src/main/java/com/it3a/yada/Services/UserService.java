package com.it3a.yada.Services;

import com.google.api.core.ApiFuture;
import com.google.cloud.firestore.*;
import com.it3a.yada.Components.DatabaseComponent;
import com.it3a.yada.Controllers.DTOs.Facebook.FbUserDTO;
import com.it3a.yada.Controllers.DTOs.UserProfileDTO;
import com.it3a.yada.GLOBAL;
import com.it3a.yada.Modals.MatchAge;
import com.it3a.yada.Modals.Score;
import com.it3a.yada.Modals.User;
import com.it3a.yada.library.GeoFire.core.GeoHash;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.*;
import java.util.concurrent.ExecutionException;

@Service
public class UserService {

    private MatchAgeService matchAgeService;
    private ScoreService scoreService;

    @Autowired
    public UserService(MatchAgeService matchAgeService, ScoreService scoreService) {
        this.matchAgeService = matchAgeService;
        this.scoreService = scoreService;
    }

    public DocumentReference getUserDocumentById(String userId) {
        Firestore database = DatabaseComponent.getDatabase();
        return database.collection("users").document(userId);
    }

    public User getUserById(String userId) {
        DocumentReference userDoc = getUserDocumentById(userId);
        ApiFuture<DocumentSnapshot> future = userDoc.get();
        DocumentSnapshot userDocument = null;
        try {
            userDocument = future.get();
        } catch (InterruptedException | ExecutionException e) {
            e.printStackTrace();
        }


        if (userDocument != null && userDocument.exists()) {
            User dbUser = userDocument.toObject(User.class);
            Objects.requireNonNull(dbUser).setId(userDocument.getId());
            // mapping geopoint manually
            dbUser.setGeoPoint(userDocument.getGeoPoint("geoPoint"));
            return dbUser;
        }
        return null;
    }

    public User createUser(User newUser) {
        MatchAge matchAge = matchAgeService.create(newUser);
        Score score = scoreService.create();
        newUser.setMatchAge(matchAge);
        newUser.setScore(score);
        newUser.setSearchDistance(GLOBAL.DEFAULT_SEARCH_DISTANCE);
        newUser.setPopularity(GLOBAL.DEFAULT_USER_POPULARITY);

        Firestore database = DatabaseComponent.getDatabase();
        CollectionReference users = database.collection("users");
        ApiFuture<DocumentReference> user = users.add(newUser);
        try {
            newUser.setId(user.get().getId());
        } catch (InterruptedException | ExecutionException e) {
            e.printStackTrace();
        }

        newUser = setUserLocation(53.2734, 7.77832031, newUser.getId(), true);
        return newUser;
    }

    public User updateUser(UserProfileDTO userProfileDTO) {
        Firestore database = DatabaseComponent.getDatabase();
        DocumentReference userDocRef = database.collection("users").document(userProfileDTO.getUserId());

        // (async) Update one field
        ApiFuture<WriteResult> updateAge = userDocRef.update("age", userProfileDTO.getAge());
        ApiFuture<WriteResult> updateLookingFrom = userDocRef.update("matchAge.lookingFrom", userProfileDTO.getAgeFrom());
        ApiFuture<WriteResult> updateLookingTo = userDocRef.update("matchAge.lookingTo", userProfileDTO.getAgeTo());
        ApiFuture<WriteResult> updateName = userDocRef.update("name", userProfileDTO.getName());
        ApiFuture<WriteResult> updateGender = userDocRef.update("gender", userProfileDTO.getGender());
        ApiFuture<WriteResult> updateGenderInterest = userDocRef.update("genderInterest", userProfileDTO.getGenderInterest());
        ApiFuture<WriteResult> updateSearchDist = userDocRef.update("searchDistance", userProfileDTO.getSearchRadius());
        // updating data in the database (flushing)
        try {
            updateAge.get();
            updateLookingFrom.get();
            updateLookingTo.get();
            updateName.get();
            updateGender.get();
            updateGenderInterest.get();
            updateSearchDist.get();
        } catch (InterruptedException | ExecutionException e) {
            e.printStackTrace();
        }

        return getUserById(userProfileDTO.getUserId());
    }

    public String setProfilePicture(String userId, String imgPath) {
        User user = getUserById(userId);
        user.setProfilePicture(imgPath);

        Firestore database = DatabaseComponent.getDatabase();
        DocumentReference userDocRef = database.collection("users").document(userId);
        userDocRef.update("profilePicture", imgPath);
        return imgPath;
    }

    public String getProfilePicture(String userId) {
        String profilePicture = getUserById(userId).getProfilePicture();
        if (profilePicture == null) {
            // no profile pic set
            profilePicture = "/assets/img/no profile picture.png";
        }
        return profilePicture;
    }

    public List<String> getPictures(String userId) {
        List<String> userPictures = getUserById(userId).getPictures();
        // lets check if user has any pictures.
        if (userPictures == null) {
            userPictures = new ArrayList<>();
        }
        return userPictures;
    }

    public String uploadPicture(String userId, String imgPath) {
        User user = getUserById(userId);
        List<String> userPictures = user.getPictures();
        // lets check if user has any pictures.
        if (userPictures == null) {
            userPictures = new ArrayList<>();
        }
        userPictures.add(imgPath);
        updateUserPictures(userPictures, userId);
        return imgPath;
    }

    public List<String> deletePicture(String userId, String imgPath) {
        List<String> userPictures = getUserById(userId).getPictures();
        userPictures.remove(imgPath);
        updateUserPictures(userPictures, userId);
        return userPictures;
    }

    public boolean updateUserLocation(double lat, double lng, String userId, boolean isLocationByGPS) {
        User user = setUserLocation(lat, lng, userId, isLocationByGPS);
        return user.getIsLocationByGPS();
    }

    private void updateUserPictures(List<String> userPictures, String userId) {
        DocumentReference userDocRef = getUserDocumentById(userId);
        userDocRef.update("pictures", userPictures);
    }

    public User setUserLocation(double lat, double lng, String userId, boolean isLocationByGPS) {
        GeoPoint geoPoint = new GeoPoint(lat, lng);
        String geoHash = new GeoHash(lat, lng).getGeoHashString();

        DocumentReference userDocRef = getUserDocumentById(userId);

        ApiFuture<WriteResult> updateGeoPoint = userDocRef.update("geoPoint", geoPoint);
        ApiFuture<WriteResult> updateGeoHash = userDocRef.update("geoHash", geoHash);
        ApiFuture<WriteResult> updateLocationByGPS = userDocRef.update("isLocationByGPS", isLocationByGPS);

        // updating data in the database (flushing)
        try {
            updateGeoPoint.get();
            updateGeoHash.get();
            updateLocationByGPS.get();
        } catch (InterruptedException | ExecutionException e) {
            e.printStackTrace();
        }

        return getUserById(userId);
    }

    // deletes user. Use it with care
    public void deleteUser(User user) {
        getUserDocumentById(user.getId()).delete();
    }

}
