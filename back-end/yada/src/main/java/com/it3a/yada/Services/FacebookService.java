package com.it3a.yada.Services;

import com.google.cloud.firestore.Firestore;
import com.google.cloud.firestore.QueryDocumentSnapshot;
import com.it3a.yada.Components.DatabaseComponent;
import com.it3a.yada.Controllers.DTOs.Facebook.FbUserDTO;
import com.it3a.yada.Modals.ScoreItems;
import com.it3a.yada.Modals.User;
import com.it3a.yada.Services.RestAPIs.FacebookDetailsClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.concurrent.ExecutionException;

@Service
public class FacebookService {

    private UserService userService;
    private FacebookDetailsClient facebookDetailsClient;

    @Autowired
    public FacebookService(UserService userService, FacebookDetailsClient facebookDetailsClient) {
        this.userService = userService;
        this.facebookDetailsClient = facebookDetailsClient;
    }

    public User authenticate(Long fbId, String fbToken) {
        return getFacebookData(fbId, fbToken);
    }

    // returns existing user
    // if user doesn't exist, it calls facebook api (Personal information) and registers the user
    private User getFacebookData(Long fbId, String fbToken) {

        User existingUser = userExists(fbId);
        if(existingUser != null) {
            return existingUser;
        }

        // making API call to facebook API
        FbUserDTO fbUserDTO = facebookDetailsClient.getFbUserDetails(fbToken);
        return createUserFromFbDTO(fbUserDTO);
    }

    public User createUserFromFbDTO(FbUserDTO fbUserDTO) {
        User newUser = new User(fbUserDTO);
        newUser.setScoreItems(getScoreItemsFromFacebook(fbUserDTO));
        return userService.createUser(newUser);
    }

    private User userExists(Long fbId) {
        Firestore database = DatabaseComponent.getDatabase();
        try {
            List<QueryDocumentSnapshot> fbUsers = database.collection("users")
                    .whereEqualTo("fbId", fbId)
                    .get()
                    .get()
                    .getDocuments();

            if (fbUsers.size() > 0) {
                String fbUserDocId = fbUsers.get(0).getId();
                return userService.getUserById(fbUserDocId);
            }
            return null;
        } catch (InterruptedException | ExecutionException e) {
            return null;
        }
    }

    private ScoreItems getScoreItemsFromFacebook(FbUserDTO fbUserDTO) {
        ScoreItems scoreItems = new ScoreItems();
        scoreItems.setLanguages(fbUserDTO.getLanguageIds());
        scoreItems.setSports(fbUserDTO.getSportsIds());
        if (fbUserDTO.getEvents() != null)
        scoreItems.setEvents(fbUserDTO.getEvents().getIds());
        if (fbUserDTO.getLikes() != null)
        scoreItems.setLikes(fbUserDTO.getLikes().getIds());
        return scoreItems;
    }
}
