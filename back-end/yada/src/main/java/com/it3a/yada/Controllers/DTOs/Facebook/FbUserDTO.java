package com.it3a.yada.Controllers.DTOs.Facebook;

import com.it3a.yada.Services.FacebookData.*;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class FbUserDTO {
    private Long id;
    private String fbToken;
    private String first_name;
    private String last_name;
    private String gender;
    private String gender_interest;
    private String email;
    private String birthday;
    private List<FbEntity> languages = new ArrayList<>();
    private FbEntityNested events;
    private FbEntityNested likes;
    private List<FbEntity> favorite_teams = new ArrayList<>();

    public List<Long> getLanguageIds() {
        return languages.stream()
                .map(FbEntity::getId)
                .collect(Collectors.toList());
    }

    public List<Long> getSportsIds() {
        return favorite_teams.stream()
                .map(FbEntity::getId)
                .collect(Collectors.toList());
    }

    public FbUserDTO() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getFbToken() {
        return fbToken;
    }

    public void setFbToken(String fbToken) {
        this.fbToken = fbToken;
    }

    public String getFirst_name() {
        return first_name;
    }

    public void setFirst_name(String first_name) {
        this.first_name = first_name;
    }

    public String getLast_name() {
        return last_name;
    }

    public void setLast_name(String last_name) {
        this.last_name = last_name;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getGender_interest() {
        return gender_interest;
    }

    public void setGender_interest(String gender_interest) {
        this.gender_interest = gender_interest;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getBirthday() {
        return birthday;
    }

    public void setBirthday(String birthday) {
        this.birthday = birthday;
    }

    public List<FbEntity> getLanguages() {
        return languages;
    }

    public void setLanguages(List<FbEntity> languages) {
        this.languages = languages;
    }

    public FbEntityNested getEvents() {
        return events;
    }

    public void setEvents(FbEntityNested events) {
        this.events = events;
    }

    public FbEntityNested getLikes() {
        return likes;
    }

    public void setLikes(FbEntityNested likes) {
        this.likes = likes;
    }

    public List<FbEntity> getFavorite_teams() {
        return favorite_teams;
    }

    public void setFavorite_teams(List<FbEntity> favorite_teams) {
        this.favorite_teams = favorite_teams;
    }
}
