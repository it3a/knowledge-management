package com.it3a.yada;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class YadaApplication {

    public static void main(String[] args) {
        SpringApplication.run(YadaApplication.class, args);
    }

}
