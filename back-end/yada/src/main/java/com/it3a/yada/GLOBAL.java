package com.it3a.yada;

public class GLOBAL {
    public final static int LOOKING_YOUNGER_THAN_USER = 2;
    public final static int LOOKING_OLDER_THAN_USER = 2;

    public static final int DEFAULT_LIKE_SCORE = 30;
    public static final int DEFAULT_EVENT_SCORE = 50;
    public static final int DEFAULT_LANGUAGE_SCORE = 90;
    public static final int DEFAULT_SPORT_SCORE = 60;

    public static final int DEFAULT_SEARCH_DISTANCE = 100;
    public static final int DEFAULT_USER_POPULARITY = 500;
    public static final int DEFAULT_USER_BEHAVIOR_CHANGE = 10;
}
