package com.it3a.yada.Controllers;

import com.it3a.yada.Modals.User;
import com.it3a.yada.Services.MatchService;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping(path="/match")
public class MatchController {

    private MatchService matchService;

    public MatchController(MatchService matchService) {
        this.matchService = matchService;
    }

    @PostMapping(path = "/get")
    public User[] getUserScore(@RequestParam String id) {
        return matchService.getUsers(id);
    }
}
