package com.it3a.yada.Controllers;

import com.it3a.yada.Modals.Message;
import com.it3a.yada.Services.MessagingService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping(path="/messaging")
public class MessagingController {

    private MessagingService messagingService;

    @Autowired
    public MessagingController(MessagingService messagingService) {
        this.messagingService = messagingService;
    }

    @PostMapping(path = "/send")
    public Message sendMessage(@RequestParam String senderId, @RequestParam String receiverId, @RequestParam String message) {
        return messagingService.sendMessage(senderId, receiverId, message);
    }

    @PostMapping(path = "/get-chat")
    public List<Message> getMessagesChat(@RequestParam String user1, @RequestParam String user2) {
        return messagingService.getMessagesChat(user1, user2);
    }
}
