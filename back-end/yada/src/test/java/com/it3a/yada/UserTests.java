package com.it3a.yada;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.it3a.yada.Controllers.DTOs.UserProfileDTO;
import com.it3a.yada.Modals.User;
import com.it3a.yada.Services.UserService;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@SpringBootTest
@AutoConfigureMockMvc
public class UserTests {

    @Autowired
    private MockMvc mvc;
    @Autowired
    private ObjectMapper objectMapper;

    @Autowired
    private UserService userService;

    private User fakeUser;

    @Before
    public void onStart() {
        User user = new User("Test user tests", "male", "female", 22);
        fakeUser = userService.createUser(user);
    }
    @After
    public void onFinish() {
        userService.deleteUser(fakeUser);
    }

    @Test
    public void getUserByIdController() throws Exception {
        // creating new user and getting its id
        String userId = fakeUser.getId();
        // testing controller
        this.mvc.perform(post("/user/get?userId=" + userId)).andExpect(status().isOk());
    }

    @Test
    public void getUserByIdService() {
        // creating new user and getting its id
        String userId = fakeUser.getId();
        // testing service
        assert userService.getUserById(userId) != null;
        assert userService.getUserById(userId).getSearchDistance() != 0;
        assert userService.getUserById(userId).getGeoPoint().getLongitude() != 0;
        assert userService.getUserById(userId).getGeoPoint().getLatitude() != 0;
        assert userService.getUserById(userId).getGeoHash().length() != 0;
        assert userService.getUserById(userId).getScore() != null;
        assert userService.getUserById(userId).getPopularity() != 0;
    }

    @Test
    public void createUser() {
        // testing if the user is actually created
        assert fakeUser.getId() != null;
        assert fakeUser.getScore() != null;
        assert fakeUser.getMatchAge() != null;
        assert fakeUser.getGeoHash() != null;
        assert fakeUser.getPopularity() == GLOBAL.DEFAULT_USER_POPULARITY;

    }

    @Test
    public void updateUserController() throws Exception {

        // creating DTO object (updated user details)
        UserProfileDTO userDto = new UserProfileDTO();
        userDto.setUserId(fakeUser.getId());
        userDto.setName("updated user");
        userDto.setGender("female");
        userDto.setGenderInterest("male");
        userDto.setAge(18);
        userDto.setAgeFrom(18);
        userDto.setAgeTo(30);

        // mapping Java object to JSON string
        String userDTO = objectMapper.writeValueAsString(userDto);

        // testing the controller
        this.mvc.perform(post("/user/update")
                .param("userDTO", userDTO)
                .content(userDTO)
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk());
    }

    @Test
    public void updateUserService() {

        // creating DTO object (updated user details)
        UserProfileDTO userDTO = new UserProfileDTO();
        userDTO.setUserId(fakeUser.getId());
        userDTO.setName("Elvis test update pass");
        userDTO.setGender("female");
        userDTO.setGenderInterest("male");
        userDTO.setAge(18);
        userDTO.setAgeFrom(18);
        userDTO.setAgeTo(30);

        User updatedUser = userService.updateUser(userDTO);

        // making sure that user was updated to the DTO we created
        assert updatedUser.getId().equals(userDTO.getUserId());
        assert updatedUser.getName().equals(userDTO.getName());
        assert updatedUser.getGender().equals(userDTO.getGender());
        assert updatedUser.getGenderInterest().equals(userDTO.getGenderInterest());
        assert updatedUser.getAge() == userDTO.getAge();
        assert updatedUser.getMatchAge().getLookingFrom() == userDTO.getAgeFrom();
        assert updatedUser.getMatchAge().getLookingTo() == userDTO.getAgeTo();
    }

    @Test
    public void updateUserLocationController() throws Exception {
        MultiValueMap<String, String> params = new LinkedMultiValueMap<>();
        params.add("userId", fakeUser.getId());
        params.add("lat", "41.83841");
        params.add("lng", "-120.21725");
        params.add("isLocationByGPS", "false");

        this.mvc.perform(post("/user/update-location")
                .params(params))
                .andExpect(status().isOk());
    }

    @Test
    public void updateUserLocation() {
        User updatedUser = userService.setUserLocation(41.83841, -120.21725, fakeUser.getId(), false);

        // making sure that user was updated to the DTO we created
        assert !updatedUser.getIsLocationByGPS();
        assert updatedUser.getGeoPoint().getLatitude() == 41.83841;
        assert updatedUser.getGeoPoint().getLongitude() == -120.21725;
    }
}
