package com.it3a.yada;

import com.it3a.yada.Modals.Behavior;
import com.it3a.yada.Modals.User;
import com.it3a.yada.Services.BehaviorService;
import com.it3a.yada.Services.UserService;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@SpringBootTest
@AutoConfigureMockMvc
public class BehaviorTests {

    @Autowired
    private MockMvc mvc;
    @Autowired
    private BehaviorService behaviorService;
    @Autowired
    private UserService userService;

    private User fakeUser;
    private User likedUser;

    @Before
    public void onStart() {
        fakeUser = userService.createUser(new User("Test user 1", "male", "female", 22));
        likedUser = userService.createUser(new User("Test user 1", "male", "female", 22));
    }

    @After
    public void onFinish() {
        userService.deleteUser(fakeUser);
        userService.deleteUser(likedUser);
    }

    @Test
    public void createBehaviorController() throws Exception {

        MultiValueMap<String, String> params = new LinkedMultiValueMap<>();
        params.add("userId", fakeUser.getId());
        params.add("chosenUserId", likedUser.getId());
        params.add("isLiked", "true");

        this.mvc.perform(post("/behavior/create")
                .params(params))
                .andExpect(status().isOk());
    }

    @Test
    public void createBehaviorService() {

        int popularityBefore = likedUser.getPopularity();
        Behavior behavior = behaviorService.createBehavior(fakeUser.getId(), likedUser.getId(), true);
        int popularityAfter = userService.getUserById(likedUser.getId()).getPopularity();

        assert behavior.getInterestedUserId() != null;
        assert behavior.getLiked();
        assert popularityBefore == GLOBAL.DEFAULT_USER_POPULARITY;
        assert popularityAfter == GLOBAL.DEFAULT_USER_POPULARITY + GLOBAL.DEFAULT_USER_BEHAVIOR_CHANGE;
    }


}
