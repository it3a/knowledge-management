package com.it3a.yada;

import com.it3a.yada.Modals.User;
import com.it3a.yada.Services.BehaviorService;
import com.it3a.yada.Services.MatchService;
import com.it3a.yada.Services.UserService;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@SpringBootTest
@AutoConfigureMockMvc
public class CoreAlgorithmTests {

    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private MatchService matchService;

    @Autowired
    private UserService userService;

    @Autowired
    private BehaviorService behaviorService;

    private User fakeUser;
    private User user;
    private User user1;
    private User user2;
    private User user3;
    private User user4;
    private User user5;

    @Before
    public void onStart() {
        fakeUser = userService.createUser(new User("TEST USER MAIN", "male", "female", 22));
        user = userService.createUser(new User("TEST USER", "female", "male", 26));
        user1 = userService.createUser(new User("TEST USER", "male", "male", 20));
        user2 = userService.createUser(new User("TEST USER MATCH", "female", "male", 21));
        user3 = userService.createUser(new User("TEST USER MATCH2", "female", "male", 21));
        user4 = userService.createUser(new User("TEST USER", "male", "female", 22));
        user5 = userService.createUser(new User("TEST USER", "male", "female", 22));
        // fake user who is looking for a date...
        userService.setUserLocation(-78.590743, -44.017228, fakeUser.getId(), false);// somewhere in antarctica
        // 81.41km from fake user. Wrong age, not a match
        userService.setUserLocation(-77.9606720882739, 45.85284672725959, user.getId(), false);
        // 81.41km from fake user. Wrong gender, not a match
        userService.setUserLocation(-77.9606720882739, -45.85284672725959, user1.getId(), false);
        // A MATCH! 81.41km from fake user
        userService.setUserLocation(-77.9606720882739, -45.85284672725959, user2.getId(), false);
        // A MATCH! 83.63km from fake user
        userService.setUserLocation(-77.97899083734744, -46.173188603275264, user3.getId(), false);
        // 153.23 km further from fake user
        userService.setUserLocation(-79.666586, -48.589463, user4.getId(), false);
        // 300km further from fake user
        userService.setUserLocation(-80.826075, -52.938126, user5.getId(), false);
    }
    @After
    public void onFinish() {
        userService.deleteUser(fakeUser);
        userService.deleteUser(user);
        userService.deleteUser(user1);
        userService.deleteUser(user2);
        userService.deleteUser(user3);
        userService.deleteUser(user4);
        userService.deleteUser(user5);
    }

    @Test
    public void matcherController() throws Exception {

        mockMvc.perform(post("/match/get")
                .param("id", fakeUser.getId())
                .contentType("application/json"))
                .andExpect(status().isOk());
    }

    @Test
    public void matcherAlgorithm() {
        // whether the user is withing the range (default is 100km range)
        // if the user is within needed age (by default is +2 and -2 current users age
        // if the user is interested in another users gender. e.g. male user interested in female
        User[] matchedUsers = matchService.getUsers(fakeUser.getId());
        assert matchedUsers.length == 2;
    }


    @Test
    // we are testing if we don't see already suggested users
    public void matcherLiked() {
        behaviorService.createBehavior(fakeUser.getId(), user2.getId(), true);
        User[] matchedUsers = matchService.getUsers(fakeUser.getId());
        assert matchedUsers.length == 1;
    }
}
