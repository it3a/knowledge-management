package com.it3a.yada;

import com.it3a.yada.Controllers.DTOs.Facebook.FbUserDTO;
import com.it3a.yada.Modals.User;
import com.it3a.yada.Services.FacebookData.FbEntity;
import com.it3a.yada.Services.FacebookService;
import com.it3a.yada.Services.MatchService;
import com.it3a.yada.Services.UserService;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.ArrayList;
import java.util.List;

@RunWith(SpringRunner.class)
@SpringBootTest
@AutoConfigureMockMvc
public class ExtrasAlgorithmTests {

    @Autowired
    private UserService userService;

    @Autowired
    private FacebookService facebookService;

    @Autowired
    private MatchService matchService;

    private User fakeUser;
    private User fbUser;
    private User fbUser2;
    private User user1;

    @Before
    public void onStart() {

        // user who is looking for a date (fb user)
        fakeUser = facebookService.createUserFromFbDTO(this.prepareFakeUser());
        // regular users (no facebook data)
        user1 = userService.createUser(new User("TEST USER ALGO", "female", "male", 20));

        // users with extra data from facebook
        fbUser = facebookService.createUserFromFbDTO(this.prepareTopFbUser());
        fbUser2 = facebookService.createUserFromFbDTO(this.prepareSecondFbUser());

        // setting users location to be at the same spot
        userService.setUserLocation(-80.826075, -52.938126, fakeUser.getId(), false);
        userService.setUserLocation(-80.826075, -52.938126, user1.getId(), false);
        userService.setUserLocation(-80.826075, -52.938126, fbUser.getId(), false);
        userService.setUserLocation(-80.826075, -52.938126, fbUser2.getId(), false);
    }

    @After
    public void onFinish() {
        userService.deleteUser(fakeUser);
        userService.deleteUser(user1);
        userService.deleteUser(fbUser);
        userService.deleteUser(fbUser2);
    }

    @Test
    public void testAlgorithmExtras() {
        // we check if the user with who I have most in common will be displayed first
        User[] matches = matchService.getUsers(fakeUser.getId());
        assert matches.length == 3;
        assert matches[0].getId().equals(fbUser.getId());
        assert matches[1].getId().equals(fbUser2.getId());
    }

    private FbUserDTO prepareFakeUser() {
        // one language match topFbUser, and one lang match second fb user
        List<FbEntity> languages = new ArrayList<>();
        languages.add(new FbEntity(1L));
        languages.add(new FbEntity(3333L));

        // two teams match topFbUser, and one team match second fb user
        List<FbEntity> favTeams = new ArrayList<>();
        favTeams.add(new FbEntity(1L));
        favTeams.add(new FbEntity(2L));
        favTeams.add(new FbEntity(2341L));

        FbUserDTO userDTO = new FbUserDTO();
        userDTO.setId(1L);
        userDTO.setFbToken("TOKEN1");
        userDTO.setFirst_name("TEST USER FB");
        userDTO.setLast_name(":)");
        userDTO.setGender("male");
        userDTO.setGender_interest("female");
        userDTO.setEmail("elv@elv.net");
        userDTO.setBirthday("06/20/1998");
        userDTO.setLanguages(languages);
        userDTO.setFavorite_teams(favTeams);
        return userDTO;
    }

    private FbUserDTO prepareTopFbUser() {
        List<FbEntity> languages = new ArrayList<>();
        languages.add(new FbEntity(1L));
        languages.add(new FbEntity(2L));
        languages.add(new FbEntity(3L));
        languages.add(new FbEntity(4L));
        languages.add(new FbEntity(5L));

        List<FbEntity> favTeams = new ArrayList<>();
        favTeams.add(new FbEntity(1L));
        favTeams.add(new FbEntity(2L));
        favTeams.add(new FbEntity(3L));
        favTeams.add(new FbEntity(5L));

        FbUserDTO userDTO = new FbUserDTO();
        userDTO.setId(1L);
        userDTO.setFbToken("TOKEN1");
        userDTO.setFirst_name("TEST USER FB");
        userDTO.setLast_name(":)");
        userDTO.setGender("female");
        userDTO.setGender_interest("male");
        userDTO.setEmail("top@elv.net");
        userDTO.setBirthday("06/20/1998");
        userDTO.setLanguages(languages);
        userDTO.setFavorite_teams(favTeams);
        return userDTO;
    }

    private FbUserDTO prepareSecondFbUser() {
        List<FbEntity> languages = new ArrayList<>();
        languages.add(new FbEntity(2222L));
        languages.add(new FbEntity(3333L));

        List<FbEntity> favTeams = new ArrayList<>();
        favTeams.add(new FbEntity(2341L));
        favTeams.add(new FbEntity(9324L));
        favTeams.add(new FbEntity(66L));

        FbUserDTO userDTO = new FbUserDTO();
        userDTO.setId(2L);
        userDTO.setFbToken("TOKEN2");
        userDTO.setFirst_name("TEST USER FB");
        userDTO.setLast_name(":)");
        userDTO.setGender("female");
        userDTO.setGender_interest("male");
        userDTO.setEmail("fb@elv.net");
        userDTO.setBirthday("06/20/1998");
        userDTO.setLanguages(languages);
        userDTO.setFavorite_teams(favTeams);
        return userDTO;
    }
}
