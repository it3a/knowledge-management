package com.it3a.yada;

import com.it3a.yada.Controllers.DTOs.Facebook.FbUserDTO;
import com.it3a.yada.Modals.ScoreItems;
import com.it3a.yada.Modals.User;
import com.it3a.yada.Services.FacebookService;
import com.it3a.yada.Services.RestAPIs.FacebookDetailsClient;
import com.it3a.yada.Services.UserService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@SpringBootTest
@AutoConfigureMockMvc
public class FacebookTests {

    private static final Long TEST_FB_ID = 111416810139860L; // theresa may fb id (test user)
    private static final String TEST_FB_TOKEN = ""; // TODO: add FB token here

    @Autowired
    private FacebookDetailsClient client;

    @Autowired
    private UserService userService;

    @Autowired
    private FacebookService facebookService;

    @Autowired
    private MockMvc mockMvc;

    @Test
    public void testFbAuthController() throws Exception {
        MultiValueMap<String, String> params = new LinkedMultiValueMap<>();
        params.add("fbId", TEST_FB_ID.toString());
        params.add("fbToken", TEST_FB_TOKEN);

        this.mockMvc.perform(post("/user/authenticate")
                .params(params))
                .andExpect(status().isOk());
    }

    @Test
    public void testGetFbUserDataAPI() {
        FbUserDTO fbUserDTO = client.getFbUserDetails(TEST_FB_TOKEN);
        assert fbUserDTO.getId() > 1;
        assert fbUserDTO.getFbToken().length() != 0;
        assert fbUserDTO.getFirst_name().length() != 0;
        assert fbUserDTO.getLast_name().length() != 0;
        assert fbUserDTO.getGender().length() != 0;
        assert fbUserDTO.getEmail().length() != 0;
        assert fbUserDTO.getBirthday().length() != 0;
        assert fbUserDTO.getLanguageIds().size() != 0;
        assert fbUserDTO.getLanguages().size() != 0;
        assert fbUserDTO.getEvents().getIds().size() != 0;
        assert fbUserDTO.getLikes().getIds().size() != 0;
        assert fbUserDTO.getFavorite_teams().size() != 0;
    }

    @Test
    public void testFbAuthService() {

        User createdUser = facebookService.authenticate(TEST_FB_ID, TEST_FB_TOKEN);
        ScoreItems scoreItems = createdUser.getScoreItems();

        assert createdUser.getId().length() != 0;
        assert createdUser.getMatchAge().getLookingFrom() > 18;
        assert createdUser.getMatchAge().getLookingTo() > 18;
        assert createdUser.getGender().length() > 1;
        assert createdUser.getName().length() > 1;
        assert createdUser.getScore() != null;
        assert createdUser.getSearchDistance() > 0;
        assert createdUser.getAge() > 18;
        assert createdUser.getPopularity() == GLOBAL.DEFAULT_USER_POPULARITY;
        assert createdUser.getFbEmail().length() > 1;
        assert createdUser.getFbId() > 0;
        assert createdUser.getFbToken().length() > 1;
        assert scoreItems.getEvents().size() > 0;
        assert scoreItems.getLanguages().size() > 0;
        assert scoreItems.getLikes().size() > 0;
        assert scoreItems.getSports().size() > 0;
        // cleaning the mess
        userService.deleteUser(createdUser);
    }
}
