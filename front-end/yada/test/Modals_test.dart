import 'dart:io';

import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:yada/Modals/User.dart';
import 'package:mockito/mockito.dart';
import 'package:http/http.dart' as http;

class MockClient extends Mock implements http.Client {}

void main() {
  test('User', () {
    final user = User(id: "0", age: 12, gender: "male", genderInterest: "female", isLocationByGPS: true, location: LatLng(80, 80), lookingFrom: 10, lookingTo: 14, name: "John Doe", searchRadius: 80);

    user.location = LatLng(0, 0);
    user.isLocationByGPS = false;
    user.searchRadius = 10;
    user.profilePicture = File("test");
    user.pictures = List<File>();
    user.pictures.add(File("test2"));

    expect(user.location, LatLng(0, 0));
    expect(user.isLocationByGPS, false);
    expect(user.searchRadius, 10);
    expect(user.pictures.length, 1);
  });
}