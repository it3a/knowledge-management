import 'package:flutter/cupertino.dart';
import 'package:flutter_driver/driver_extension.dart';
import 'mock_client.dart';
import 'package:yada/main.dart';
import 'package:http/testing.dart';

void main() {
  // This line enables the extension
  enableFlutterDriverExtension();
  WidgetsApp.debugAllowBannerOverride = false; // remove debug banner

  final MockClient client = integrationTestMockClient;

  // Call the `main()` function of your app or call `runApp` with any widget you
  // are interested in testing.
  runApp(MyApp(client));
}