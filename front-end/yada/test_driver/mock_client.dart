import 'package:http/testing.dart';
import 'package:http/http.dart';
import 'package:yada/GLOBALS.dart';


MockClient integrationTestMockClient = MockClient((request) async {
  switch (request.url.toString()) {
    case GLOBALS.SERVERADDRESS + 'user/get':
      return Response('''{
      "id": "3lUOJJZhwtg4eQWROUgc",
          "matchAge": {
          "lookingFrom": 35,
          "lookingTo": 39
          },
          "description": null,
          "gender": "Female",
          "genderInterest": "Male",
          "geoHash": "u1kukcqet7",
          "name": "Theresa May",
          "pictures": [],
          "behaviors": null,
          "profilePicture": null,
          "score": {
          "like": 30,
          "event": 50,
          "language": 90,
          "sport": 60
          },
          "scoreItems": {
          "likes": null,
          "events": null,
          "languages": null,
          "matches": null,
          "sports": null
          },
          "searchDistance": 100,
          "age": 37,
          "popularity": 500,
          "isLocationByGPS": false,
          "fbId": 108162483786402,
          "fbToken": "EAAHVZCwexVHsBAA58Qo9KwT1RU2inpiLjZAGRAPeJ9KQOi1FdZBZAYuPpBptRy6J3QEI4OtQbfHJaZCZBXfFKQ9PZBa4ZAIC1AB",
          "fbEmail": "theresa_gbgnlpb_may@tfbnw.net"
      }''', 200);
    case GLOBALS.SERVERADDRESS + 'messaging/send':
      return Response('''{
      "id": "abcdefgh123chatmessage",
      "senderId": "3lUOJJZhwtg4eQWROUgc",
      "receiverId": "",
      "message": "Success!",
      "time": {
        "seconds": 1562140719
      }
    }''', 200);
    case GLOBALS.SERVERADDRESS + 'match/get':
      return Response('''[
    {
      "id": "100",
      "matchAge": {
        "lookingFrom": 35,
        "lookingTo": 39
      },
      "description": null,
      "gender": "Male",
      "genderInterest": "Female",
      "geoHash": "u1kukcqet7",
      "name": "Boris",
      "pictures": [],
      "behaviors": null,
      "profilePicture": null,
      "score": {
        "like": 30,
        "event": 50,
        "language": 90,
        "sport": 60
      },
      "scoreItems": {
        "likes": null,
        "events": null,
        "languages": null,
        "matches": null,
        "sports": null
      },
      "searchDistance": 100,
      "age": 37,
      "popularity": 500,
      "isLocationByGPS": false
    },
    {
      "id": "200",
      "matchAge": {
        "lookingFrom": 35,
        "lookingTo": 39
      },
      "description": null,
      "gender": "Male",
      "genderInterest": "Female",
      "geoHash": "u1kukcqet7",
      "name": "Brexit",
      "pictures": [],
      "behaviors": null,
      "profilePicture": null,
      "score": {
        "like": 30,
        "event": 50,
        "language": 90,
        "sport": 60
      },
      "scoreItems": {
        "likes": null,
        "events": null,
        "languages": null,
        "matches": null,
        "sports": null
      },
      "searchDistance": 100,
      "age": 37,
      "popularity": 500,
      "isLocationByGPS": false
    },
    {
      "id": "300",
      "matchAge": {
        "lookingFrom": 35,
        "lookingTo": 39
      },
      "description": null,
      "gender": "Male",
      "genderInterest": "Female",
      "geoHash": "u1kukcqet7",
      "name": "Macron",
      "pictures": [],
      "behaviors": null,
      "profilePicture": null,
      "score": {
        "like": 30,
        "event": 50,
        "language": 90,
        "sport": 60
      },
      "scoreItems": {
        "likes": null,
        "events": null,
        "languages": null,
        "matches": null,
        "sports": null
      },
      "searchDistance": 100,
      "age": 37,
      "popularity": 500,
      "isLocationByGPS": false
    }
]''', 200);
    case GLOBALS.SERVERADDRESS + 'behavior/create':
      return Response('', 200);
    case GLOBALS.SERVERADDRESS + 'behavior/get-matches':
      return Response('''[
{
	"userId": "400",
	"name": "Horst",
	"description": "Its a nice Horst.",
	"gender": "Male",
	"age": 38
},
{
	"userId": "500",
	"name": "Manfred",
	"description": "Its a nice Manfred.",
	"gender": "Male",
	"age": 36
}
]''', 200);
    case GLOBALS.SERVERADDRESS + 'messaging/get-chat':
      return Response('''[
{
	"id": "111",
	"senderId": "400",
	"receiverId": "3lUOJJZhwtg4eQWROUgc",
	"message": "TestMessage",
	"time": {
		"seconds": 1562140519
	}
},
{
	"id": "222",
	"senderId": "500",
	"receiverId": "3lUOJJZhwtg4eQWROUgc",
	"message": "TestMessage2",
	"time": {
		"seconds": 1562140619
	}
}
]''', 200);
    case GLOBALS.SERVERADDRESS + 'user/update':
      return Response('''{
	"id": "3lUOJJZhwtg4eQWROUgc",
	"matchAge": {
		"lookingFrom": 55,
		"lookingTo": 65
	},
	"description": null,
	"gender": "Male",
	"genderInterest": "Female",
	"geoHash": "u1kukcqet7",
	"name": "Angela Merkel",
	"pictures": [],
	"behaviors": null,
	"profilePicture": null,
	"score": {
		"like": 30,
		"event": 50,
		"language": 90,
		"sport": 60
	},
	"scoreItems": {
		"likes": null,
		"events": null,
		"languages": null,
		"matches": null,
		"sports": null
	},
	"searchDistance": 50,
	"age": 60,
	"popularity": 500,
	"isLocationByGPS": false,
	"fbId": 108162483786402,
	"fbToken": "EAAHVZCwexVHsBAA58Qo9KwT1RU2inpiLjZAGRAPeJ9KQOi1FdZBZAYuPpBptRy6J3QEI4OtQbfHJaZCZBXfFKQ9PZBa4ZAIC1AB",
	"fbEmail": "theresa_gbgnlpb_may@tfbnw.net"
}''', 200);
    default:
      return Response('', 404);
  }
});