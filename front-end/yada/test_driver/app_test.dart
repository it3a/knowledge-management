import 'dart:io';

import 'package:flutter_driver/flutter_driver.dart';
import 'package:test/test.dart';

void main() {

  ///EXECUTE TEST WITH: flutter drive --target=test_driver/app.dart
  ///from yada root folder.
  group('Yada', () {
    Duration timeoutInterval = Duration(seconds: 2);
    FlutterDriver driver;

    // Connect to the Flutter driver before running any tests
    setUpAll(() async {
      await Process.run('adb' , ['shell' ,'pm', 'grant', 'com.it3a.yada', 'android.permission.ACCESS_COARSE_LOCATION']);
      await Process.run('adb' , ['shell' ,'pm', 'grant', 'com.it3a.yada', 'android.permission.ACCESS_FINE_LOCATION']);
      await Process.run('adb' , ['shell' ,'pm', 'grant', 'com.it3a.yada', 'android.permission.READ_EXTERNAL_STORAGE']);
      driver = await FlutterDriver.connect();
    });

    // Close the connection to the driver after the tests have completed
    tearDownAll(() async {
      if (driver != null) {
        driver.close();
      }
    });

    test('Login', () async {
      ///SPLASH SCREEN TEST
      await driver.tap(find.byValueKey('button_loginById'), timeout: timeoutInterval);

      ///LOGIN SCREEN TEST
      await driver.tap(find.byValueKey('textField_docId'), timeout: timeoutInterval);
      await driver.enterText('3Plo5i46BDdTYFEMhSeV', timeout: timeoutInterval);
      await driver.waitFor(find.text('3Plo5i46BDdTYFEMhSeV'), timeout: timeoutInterval);
      await driver.tap(find.byValueKey('button_login'), timeout: timeoutInterval);
    });

    test('Behavior', () async {
      ///BEHAVIOR SCREEN TEST
      await driver.waitFor(find.text('Boris'), timeout: timeoutInterval);
      await driver.tap(find.byValueKey('button_like'), timeout: timeoutInterval);
      await driver.waitFor(find.text('Brexit'), timeout: timeoutInterval);
      await driver.tap(find.byValueKey('button_dislike'), timeout: timeoutInterval);
      await driver.waitFor(find.text('Macron'), timeout: timeoutInterval);
    });

    test('Matched', () async {
      ///MATCHED SCREEN TEST
      await driver.tap(find.byValueKey('button_matchedScreen'), timeout: timeoutInterval);
      await driver.waitFor(find.text('Horst'), timeout: timeoutInterval);
      await driver.waitFor(find.text('Manfred'), timeout: timeoutInterval);
      await driver.tap(find.text('Horst'), timeout: timeoutInterval);
    });

    test('Chat', () async {
      ///CHAT SCREEN TEST
      await driver.waitFor(find.text('TestMessage'), timeout: timeoutInterval);
      await driver.tap(find.byValueKey('textField_chatInput'), timeout: timeoutInterval);
      await driver.enterText('Success!', timeout: timeoutInterval);
      await driver.waitFor(find.text('Success!'), timeout: timeoutInterval);
      await driver.tap(find.byValueKey('button_sendMessage'), timeout: timeoutInterval);
      await driver.waitFor(find.text('Success!'), timeout: timeoutInterval);
    });

    test('Profile', () async {
      ///Profile SCREEN
      await driver.tap(find.byValueKey('button_backChat'), timeout: timeoutInterval);
      await driver.tap(find.byValueKey('button_backMatched'), timeout: timeoutInterval);
      await driver.tap(find.byValueKey('button_profileScreen'), timeout: timeoutInterval);
      await driver.waitFor(find.text('Theresa May, 37'), timeout: timeoutInterval);
    });

    test('Edit profile', () async {
      ///Edit Profile SCREEN
      await driver.tap(find.byValueKey('button_editProfile'), timeout: timeoutInterval);
      await driver.waitFor(find.text('Edit Profile'), timeout: timeoutInterval);

      await driver.tap(find.byValueKey('textField_name'), timeout: timeoutInterval);
      await driver.enterText('Angela Merkel', timeout: timeoutInterval);
      await driver.waitFor(find.text('Angela Merkel'), timeout: timeoutInterval);

      await driver.tap(find.byValueKey('textField_age'), timeout: timeoutInterval);
      await driver.enterText('60', timeout: timeoutInterval);
      await driver.waitFor(find.text('60'), timeout: timeoutInterval);

      await driver.tap(find.byValueKey('textField_ageFrom'), timeout: timeoutInterval);
      await driver.enterText('55', timeout: timeoutInterval);
      await driver.waitFor(find.text('55'), timeout: timeoutInterval);

      await driver.tap(find.byValueKey('textField_ageTo'), timeout: timeoutInterval);
      await driver.enterText('65', timeout: timeoutInterval);
      await driver.waitFor(find.text('65'), timeout: timeoutInterval);

      await driver.tap(find.byValueKey('button_update'), timeout: timeoutInterval);
    });

    test('Check updated profile', () async {
      ///Profile Screen
      await driver.tap(find.byValueKey('button_backUpdateProfile'), timeout: timeoutInterval);
      await driver.waitFor(find.text('Angela Merkel, 60'), timeout: timeoutInterval);

      ///Edit Profile screen
      await driver.tap(find.byValueKey('button_editProfile'), timeout: timeoutInterval);
      await driver.waitFor(find.text('Angela Merkel'), timeout: timeoutInterval);
      await driver.waitFor(find.text('60'), timeout: timeoutInterval);
      await driver.waitFor(find.text('55'), timeout: timeoutInterval);
      await driver.waitFor(find.text('65'), timeout: timeoutInterval);
      await driver.waitFor(find.text('Select search radius: 50 km'), timeout: timeoutInterval);
    });

    test('Logout', () async {
      ///Profile Screen
      await driver.tap(find.byValueKey('button_backUpdateProfile'), timeout: timeoutInterval);
      await driver.waitFor(find.text('Angela Merkel, 60'), timeout: timeoutInterval);

      ///Splash SCREEN
      await driver.tap(find.byValueKey('button_logout'), timeout: timeoutInterval);
      await driver.waitFor(find.text('Yet Another Dating App'), timeout: timeoutInterval);
    });
  });
}