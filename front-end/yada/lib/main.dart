
import 'dart:io';

import 'package:firebase_core/firebase_core.dart';
import 'package:firebase_storage/firebase_storage.dart';
import 'package:flutter/material.dart';
import 'package:yada/GLOBALS.dart';
import 'package:yada/screens/SplashScreen.dart';
import 'package:http/http.dart' as http;

void main() async {
  final FirebaseApp app = await FirebaseApp.configure(
    name: 'yada-development',
    options: FirebaseOptions(
      googleAppID: Platform.isIOS
          ? '1:159623150305:ios:4a213ef3dbd8997b'
          : '1:300725494073:android:e9eb12bd1b7f96e3',
      gcmSenderID: '300725494073',
      apiKey: 'AIzaSyCQjDgycXK0cmx77n1FcM1D8Rf5ERmxsLc',
      projectID: 'yada-6c0ff',
    ),
  );

  GLOBALS.firebaseStorage = FirebaseStorage(storageBucket: 'gs://yada-6c0ff.appspot.com');

  runApp(MyApp(new http.Client()));
}

class MyApp extends StatelessWidget {

  MyApp(http.Client client) {
    GLOBALS.httpClient = client;
  }

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
        theme: ThemeData(
          primaryColor: GLOBALS.yadaMain,
        ),
      home: Scaffold(
        body: SplashScreen(),
      ),
    );
  }
}
