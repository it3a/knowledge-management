import 'dart:convert';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'package:yada/GLOBALS.dart';
import 'package:yada/Modals/User.dart';
import 'package:yada/screens/BehaviorScreen.dart';
import 'package:yada/screens/UserLocationScreen.dart';

class UpdateForm extends StatefulWidget {
  final bool userCreated;
  UpdateForm({Key key, @required this.userCreated}) : super(key: key);

  @override
  UpdateFormState createState() => new UpdateFormState(userCreated);
}

class UpdateFormState extends State<UpdateForm> {
  final _formKey = GlobalKey<FormState>();
  final bool userCreated;
  int searchRadiusValue = 5;
  String userGenderValue;
  String genderInterestValue;

  UpdateFormState(this.userCreated);

  void _onChanged(double value){
    setState(() {
      searchRadiusValue = value.round();
    });
  }

  final nameController = TextEditingController();
  final ageController = TextEditingController();
  final ageFromController = TextEditingController();
  final ageToController = TextEditingController();

  @override
  void dispose() {
    // Clean up the controller when the Widget is removed from the Widget tree
    nameController.dispose();
    ageController.dispose();
    ageFromController.dispose();
    ageToController.dispose();
    super.dispose();
  }

  handleUserResponse(User user) {
    nameController.text = user.name;
    ageController.text = user.age.toString();
    ageFromController.text = user.lookingFrom.toString();
    ageToController.text = user.lookingTo.toString();
    if (user.searchRadius < 5 || user.searchRadius > 100) {
      user.searchRadius = 5;
    }
    searchRadiusValue = user.searchRadius;
    userGenderValue = user.gender;
    genderInterestValue = user.genderInterest;
    return user;
  }

  @override
  void initState() {
    super.initState();
    handleUserResponse(GLOBALS.currentUser);
    setLastGenderLabels();

  }

  handleUpdate() async {
    String name = nameController.text;
    String gender = userGenderValue;
    String genderInterest = genderInterestValue;
    String age = ageController.text;
    String ageFrom = ageFromController.text;
    String ageTo = ageToController.text;
    String searchRadius = searchRadiusValue.toString();
    String url = GLOBALS.SERVERADDRESS + 'user/update';

    var response = await GLOBALS.httpClient.post(url,
        headers:{
          'Content-type' : 'application/json',
          'Accept': 'application/json',
        },
        body: json.encode({
          'userId': GLOBALS.currentUser.id,
          'name': name,
          'gender': gender,
          'genderInterest': genderInterest,
          'age': age.toString(),
          'ageFrom': ageFrom.toString(),
          'ageTo': ageTo.toString(),
          'searchRadius':searchRadius.toString()
        }));

    if (response.statusCode == 200) {
      GLOBALS.currentUser = handleUserResponse(User.fromJson(json.decode(response.body)));
      //Scaffold.of(context).showSnackBar(SnackBar(content: Text('User data updated')));
    } else {
      //Scaffold.of(context).showSnackBar(SnackBar(content: Text('Failed to update user data')));
    }
  }

  @override
  Widget build(BuildContext context) {

    return Scaffold(
      appBar: AppBar(
        automaticallyImplyLeading: false,
        leading: IconButton(icon: Icon(Icons.arrow_back),
          key: Key('button_backUpdateProfile'),
          onPressed: (){
          if (userCreated) {
            Navigator.push(context,
                MaterialPageRoute(builder: (context) => BehaviorScreen()));
          } else {
            Navigator.pop(context);
          }
          },
        ),
        title: Text('Edit Profile'),
        actions: <Widget>[
          new IconButton(
            icon: new Icon(Icons.location_on),
            onPressed: () {
              Navigator.push(context,
                  MaterialPageRoute(builder: (context) => UserLocationScreen()));
            },
          )
        ],
      ),
      body: Form(
        key: _formKey,
        child: SingleChildScrollView(
          child: Padding(
            padding: const EdgeInsets.all(15.0),
            child: Column(

              children: <Widget>[
                Container(
                  margin: const EdgeInsets.only(top: 20.0),
                  width: 380,
                  child:TextFormField(
                    key: Key('textField_name'),
                    decoration: InputDecoration(hintText: "Name",
                      labelText: 'Name',
                      border: OutlineInputBorder(),
                    ),
                    controller: nameController,
                  )
                ),
                SizedBox(
                  height: 15,
                ),

                Container(
                  width: 380,
                  child: Row(
                    children: <Widget>[
                      Flexible(
                        child: Padding(
                          padding: const EdgeInsets.only(right: 10.0),
                          child: new FormField(
                            builder: (FormFieldState state) {
                              return InputDecorator(
                                decoration: InputDecoration(
                                  border: OutlineInputBorder(),
                                  labelText: 'My Gender',
                                ),
                                child: new DropdownButtonHideUnderline(
                                  child: new DropdownButton(
                                    value: _currentGender,
                                    items: _dropDownMenuItemsGender,
                                    isDense: true,
                                    onChanged: changedGenderDropDownItem,
                                  ),
                                ),
                              );
                            },
                          ),
                        )
                      ),

                      Flexible(
                          child: Padding(
                            padding: const EdgeInsets.only(left: 10.0),
                            child: new FormField(
                              builder: (FormFieldState state) {
                                return InputDecorator(
                                  decoration: InputDecoration(
                                    border: OutlineInputBorder(),
                                    labelText: 'Gender interested',

                                  ),
                                  child: new DropdownButtonHideUnderline(
                                    child: new DropdownButton(
                                      value: _currentInterestedGender,
                                      items: _dropDownMenuItemsGenderInterested,
                                      isDense: true,
                                      onChanged: changedInterestedGenderDropDownItem,
                                    ),
                                  ),
                                );
                              },
                            ),
                          )
                      ),
                    ],
                  ),
                ),

                Container(
                    margin: const EdgeInsets.only(top: 20.0),
                    width: 380,
                    child: TextFormField(
                      key: Key('textField_age'),
                      decoration: InputDecoration(
                        labelText: 'My Age',
                        border: OutlineInputBorder(),
                      ),
                      controller: ageController,
                      keyboardType: TextInputType.number,
                    ),
                ),

                Container(
                  margin: const EdgeInsets.only(top: 20.0, bottom: 20),
                  width: 380,
                  child: Row(
                    //mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: <Widget>[
                      Flexible(
                        child: Padding(
                          padding: const EdgeInsets.only(right: 10.0),
                          child: TextFormField(
                            key: Key('textField_ageFrom'),
                            decoration: InputDecoration(
                              labelText: 'Age interested from',
                              border: OutlineInputBorder(),
                            ),
                            controller: ageFromController,
                            keyboardType: TextInputType.number,
                          ),
                        ),
                      ),

                      Flexible(
                        child: Padding(
                          padding: const EdgeInsets.only(left: 10.0),
                          child: TextFormField(
                            key: Key('textField_ageTo'),
                            decoration: InputDecoration(
                              labelText: 'Age interested to',
                              border: OutlineInputBorder(),
                            ),
                            controller: ageToController,
                            keyboardType: TextInputType.number,
                          ),
                        ),
                      ),
                    ],
                  ),
                ),

                Text("Select search radius: ${searchRadiusValue.round()}" +" km",
                textAlign: TextAlign.left,),
                Slider(
                  min: 5.0,
                  max: 100.0,
                  value: searchRadiusValue.toDouble(),
                onChanged: (double value){
                    _onChanged(value);
                    },
                ),
                SizedBox(
                    width: double.infinity,
                    height: 50,
                    child: RaisedButton(
                      key: Key('button_update'),
                      onPressed: () {
                        if (_formKey.currentState.validate()) {
                          handleUpdate();
                        }
                      },
                      color: GLOBALS.yadaMain,
                      child: Text('Update',
                        style: TextStyle(color: GLOBALS.secondaryTextColor),

                      ),
                    )
                )

              ],
            ),
          ),
        ),
      ),
    );
  }
  List _genders = ['Male', 'Female'];

  //Users gender
  List<DropdownMenuItem<String>> _dropDownMenuItemsGender;
  String _currentGender;

  //Genders interested in
  List<DropdownMenuItem<String>> _dropDownMenuItemsGenderInterested;
  String _currentInterestedGender;

  //Genders Dropdown Menu
  List<DropdownMenuItem<String>> getDropDownMenuItemsGender(){
    List<DropdownMenuItem<String>> items = new List();
    for(String gender in _genders){
      items.add(new DropdownMenuItem(
          value: gender,
          child: new Text(gender),
        ));
    }
    return items;
  }

  //Set 'User gender' and 'Gender interested'
  void setLastGenderLabels(){
    _dropDownMenuItemsGender = getDropDownMenuItemsGender();
    _dropDownMenuItemsGenderInterested = getDropDownMenuItemsGender();

    if(userGenderValue == 'Male' || userGenderValue == 'male'){
      _currentGender = _dropDownMenuItemsGender[0].value;
    }
    else{
      _currentGender = _dropDownMenuItemsGender[1].value;
    }

    if(genderInterestValue == 'Male' || genderInterestValue == 'male'){
      _currentInterestedGender = _dropDownMenuItemsGenderInterested[0].value;
    }
    else{
      _currentInterestedGender = _dropDownMenuItemsGenderInterested[1].value;
    }
  }

  void changedGenderDropDownItem(String selectedGender){
    setState(() {
      _currentGender = selectedGender;
      userGenderValue = _currentGender;
    });
  }

  void changedInterestedGenderDropDownItem(String selectedGender){
    setState(() {
      _currentInterestedGender = selectedGender;
      genderInterestValue = _currentInterestedGender;
    });
  }
}
