import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'package:yada/GLOBALS.dart';
import 'package:yada/Modals/User.dart';

import 'BehaviorScreen.dart';

void main() => runApp(RegistrationScreen());

class RegistrationScreen extends StatefulWidget {
  @override
  RegistrationFormState createState() {
    return RegistrationFormState();
  }
}

class RegistrationFormState extends State<RegistrationScreen> {
  final _formKey = GlobalKey<FormState>();

  @override
  Widget build(BuildContext context) {
    String name;
    String gender;
    String genderInterest;
    var age;

    // Build a Form widget using the _formKey we created above
    return Scaffold(
      appBar: AppBar(
        title: Text('Register'),
      ),
      body: Form(
        key: _formKey,

        child: SingleChildScrollView(
          child: Container(
            padding: const EdgeInsets.all(30.0),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                TextField(
                  onChanged: (value) {
                    name = value;
                  },
                  decoration: InputDecoration(
                      hintText: "First Name",
                      border: OutlineInputBorder()
                  ),
                ),
                 GLOBALS.spacer,
                TextField(
                  onChanged: (value) {
                    gender = value;
                  },
                  decoration: InputDecoration(
                      hintText: "Gender",
                      border: OutlineInputBorder()),

                ),
                GLOBALS.spacer,
                TextField(
                  onChanged: (value) {
                    genderInterest = value;
                  },
                  decoration: InputDecoration(
                      hintText: "Gender interest",
                      border: OutlineInputBorder()
                  ),
                ),
                GLOBALS.spacer,
                TextField(
                  onChanged: (value) {
                    age = value;
                  },
                  decoration: InputDecoration(
                      hintText: "Age",
                      border: OutlineInputBorder()
                  ),
                  keyboardType: TextInputType.number,
                ),
                GLOBALS.spacer,
                SizedBox(
                  width: double.infinity ,
                  height: 50,
                  child: RaisedButton(
                    color: GLOBALS.yadaMain,
                    onPressed: () => registerUser(name, gender, genderInterest, age),
                    child: Text('Register', style: TextStyle(color: GLOBALS.secondaryTextColor),),
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }

  registerUser(name, gender, genderInterest, age) async {

    String url = GLOBALS.SERVERADDRESS + 'user/create';

    var response = await GLOBALS.httpClient.post(url,
        headers:{
          'Content-type' : 'application/json',
          'Accept': 'application/json',
        },
        body: json.encode({
          'name': name,
          'gender': gender,
          'genderInterest': genderInterest,
          'age': age,
        }));

    if (response.statusCode == 200) {
      // get user object from the response
      User user =  User.fromJson(json.decode(response.body));
      GLOBALS.currentUser = user;

      // navigate to another screen and pass new user
      Navigator.push(
          context,
          MaterialPageRoute(
            builder: (context) => BehaviorScreen(),
          ));
    } else {
      // If that response was not OK
      Scaffold.of(context).showSnackBar(SnackBar(
          content: Text(
              'Error trying to register the user... Try agian later.')));
    }
  }
}
