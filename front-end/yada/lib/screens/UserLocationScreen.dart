import 'dart:async';

import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter_google_places/flutter_google_places.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:google_maps_webservice/places.dart';
import 'package:yada/Controller/UserController.dart';
import 'package:yada/Controller/UserInterfaceController.dart';
import 'package:yada/GLOBALS.dart';

class UserLocationScreen extends StatefulWidget {
  @override
  UserLocationScreenState createState() => new UserLocationScreenState();
}

class UserLocationScreenState extends State<UserLocationScreen> {

  final searchFormKey = GlobalKey<FormState>();
  final searchController = TextEditingController();
  final GoogleMapsPlaces placesController = new GoogleMapsPlaces(apiKey: GLOBALS.MAPSAPIKEY);
  BuildContext scaffoldContext;

  final CameraPosition initialMapsLocation = CameraPosition(
    target: GLOBALS.currentUser.location,
    zoom: 17,
  );

  Map<MarkerId, Marker> markers = <MarkerId, Marker>{};
  GoogleMapController mapsController;

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Location"),
        actions: <Widget>[
          new IconButton(
            icon: new Icon(Icons.search),
            onPressed: () => handleSearchButton(),
          ),
          new IconButton(
            icon: new Icon(Icons.gps_fixed),
            onPressed: () => handleGpsButton(),
          )
        ],
      ),

      body: Builder(
        builder: (scaffoldContext) {
          this.scaffoldContext = scaffoldContext;
          return buildView();
        },
      ),
    );
  }

  Widget buildView() {
    return Column(
      children: <Widget>[
        Flexible(
          child: GoogleMap(
            mapType: MapType.normal,
            initialCameraPosition: initialMapsLocation,
            markers: Set<Marker>.of(markers.values),
            onMapCreated: (GoogleMapController controller) {
              mapsController = controller;
              setLocationMarker();
            },
          ),
        )
      ],
    );
  }

  Future<void> handleSearchButton() async {
    try {
      Prediction p = await PlacesAutocomplete.show(
          context: context,
          apiKey: GLOBALS.MAPSAPIKEY,
          onError: (response) => UserInterfaceController.showError(scaffoldContext, "An error occured while displaying the location search map."),
          mode: Mode.fullscreen,
          language: "en",
          location: GLOBALS.currentUser.location == null ? null : Location(GLOBALS.currentUser.location.latitude, GLOBALS.currentUser.location.longitude),
          radius: GLOBALS.currentUser.location == null ? null : 10000
      );

      PlacesDetailsResponse place = await placesController.getDetailsByPlaceId(p.placeId);
      LatLng placeLocation = LatLng(place.result.geometry.location.lat, place.result.geometry.location.lng);

      GLOBALS.currentUser.location = placeLocation;
      setLocationMarker();
      UserController.updateLocation(scaffoldContext, false);
    } catch (e) {
      UserInterfaceController.showError(scaffoldContext, "An error occured while displaying the location search.");
    }
  }

  void handleGpsButton() async {
    await UserController.getUserLocation();
    setLocationMarker();
    UserController.updateLocation(scaffoldContext, true);
  }

  void setLocationMarker() {
    MarkerId markerId = new MarkerId("currentLoc");

    setState(() {
      markers[markerId] = new Marker(
        markerId: markerId,
        position: GLOBALS.currentUser.location,
        infoWindow: InfoWindow(title: "Current location"),
      );
    });

    mapsController.animateCamera(CameraUpdate.newCameraPosition(
      CameraPosition(
          target: GLOBALS.currentUser.location,
          zoom: 17.0
      ),
    ));
  }
}