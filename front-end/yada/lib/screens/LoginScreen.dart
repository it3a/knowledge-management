import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter_facebook_login/flutter_facebook_login.dart';
import 'package:http/http.dart' as http;
import 'package:yada/Controller/UserController.dart';
import 'package:yada/GLOBALS.dart';
import 'package:yada/Modals/User.dart';

import 'BehaviorScreen.dart';

class LoginScreen extends StatefulWidget {
  @override
  LoginFormState createState() {
    return LoginFormState();
  }
}

class LoginFormState extends State<LoginScreen> {
  final _formKey = GlobalKey<FormState>();
  final TextEditingController documentIdController = TextEditingController();
  BuildContext scaffoldContext;

  @override
  Widget build(BuildContext context) {
    String docId;

    // Build a Form widget using the _formKey we created above
    return Scaffold(
      appBar: AppBar(
        title: Text('Login'),
      ),
      body: Builder(
        builder: (scaffoldContext) {
          this.scaffoldContext = scaffoldContext;
          return Form(
            key: _formKey,
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                TextField(
                  key: Key('textField_docId'),
                  controller: documentIdController,
                  onChanged: (value) {
                    docId = value;
                  },
                  decoration: InputDecoration(hintText: "Document Id"),
                ),
                ButtonBar(
                  children: <Widget>[
                    Padding(
                      padding: EdgeInsets.symmetric(horizontal: 0),
                      child: RaisedButton(
                        key: Key('button_login'),
                        onPressed: () =>
                            loginUser(documentIdController.text),
                        color: Colors.green,
                        child: Text('Login',
                          style: TextStyle(color: Colors.white),
                        ),
                      ),
                    ),
                  ],
                )
              ],
            ),
          );
        },
      ),
    );
  }

  loginUser(docId) async {
    String url = GLOBALS.SERVERADDRESS + 'user/get';

    var response = await GLOBALS.httpClient.post(url, body: {
      'userId': docId
    });

    if (response.statusCode == 200) {
      // get user object from the response
      User user =  User.fromJson(json.decode(response.body));
      GLOBALS.currentUser = user;
      UserController.fillPictures(null);

      UserController.getMatchedUsers();
      // navigate to another screen and pass new user
      Navigator.push(
          context,
          MaterialPageRoute(
            builder: (context) => BehaviorScreen(),
          ));
    } else {
      // If that response was not OK
      Scaffold.of(context).showSnackBar(SnackBar(
          content: Text(
              'Error trying to login with this document id')));
    }
  }


}
