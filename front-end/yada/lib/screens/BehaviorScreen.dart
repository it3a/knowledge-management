import 'dart:collection';
import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:http/http.dart' as http;
import 'package:permission_handler/permission_handler.dart';
import 'package:yada/Controller/UserController.dart';
import 'package:yada/Controller/UserInterfaceController.dart';
import 'package:yada/GLOBALS.dart';
import 'package:yada/Modals/User.dart';
import 'package:yada/screens/AboutUserScreen.dart';

import 'MatchedScreen.dart';
import 'UserProfileScreen.dart';

class BehaviorScreen extends StatefulWidget {
  @override
  _BehaviorScreenState createState() => _BehaviorScreenState();
}

class _BehaviorScreenState extends State<BehaviorScreen> {
  final _formKey = GlobalKey<FormState>();
  BuildContext scaffoldContext;
  bool isLiked;
  Queue<User> users;
  User userToShow;
  bool isError = false;
  bool isLoading = true;
  bool noMatches = false;
  bool gpsDenied = false;
  PermissionStatus _status;

  //TODO image display test - Delete imageplaceholder later
  String imagePlaceholder;


  @override
  void initState() {
    super.initState();

    if (GLOBALS.currentUser.isLocationByGPS) {
      UserController.getUserLocation();
    }

    PermissionHandler().checkPermissionStatus(PermissionGroup.locationWhenInUse)
        .then(updatePermissionStatus);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        automaticallyImplyLeading: false,
        title: Text("Find a date..."),
        leading: IconButton(icon: Icon(Icons.person),
        key: Key('button_profileScreen'),
        onPressed: (){
          Navigator.push(context,
              MaterialPageRoute(builder: (context) => UserProfileScreen()));
          },
        ),
        actions: <Widget>[
          new IconButton(
            key: Key('button_matchedScreen'),
            icon: new Icon(Icons.chat),
            onPressed: () {
              Navigator.push(context,
                  MaterialPageRoute(builder: (context) => MatchedScreen()));
            },
          )
        ],
      ),
      body: Builder(
        builder: (scaffoldContext) {
          this.scaffoldContext = scaffoldContext;
          return buildMatchContainer();
        },
      ),
    );
  }

  Widget buildMatchContainer() {
    if(isError) {
      return Center(
          child: Column(
            mainAxisSize: MainAxisSize.min,
            children: <Widget>[
              Text("Error..."),
            ],
          ),
        );
    }

    if(isLoading) {
      return Scaffold(
        body: Center(
          child: Column(
            mainAxisSize: MainAxisSize.min,
            children: <Widget>[
              CircularProgressIndicator(
                valueColor: AlwaysStoppedAnimation<Color>(GLOBALS.yadaMain),
              )
            ],
          ),
        )
      );
    }

    if(noMatches) {
      return Scaffold(
          body: Center(
          child: Column(
            mainAxisSize: MainAxisSize.min,
            children: <Widget>[
              Text("No matches found..."),
            ],
          ),
        ));
    }

    if(gpsDenied) {
      return Scaffold(
          body: Center(
          child: Column(
            mainAxisSize: MainAxisSize.min,
            children: <Widget>[
              Text("Serivce location was denied!"),
              Text("To get Matches enable GPS Access and restart the App! "),
              RaisedButton(
                child: Text('Enable GPS'),
                onPressed: () => PermissionHandler().openAppSettings(),
                color: Colors.white70,
              ),
            ],
          ),
        ));
    }

    BoxDecoration boxDeco = new BoxDecoration();
    if (userToShow.profilePicture != null) {
      boxDeco = new BoxDecoration(
        image: new DecorationImage(
          image: new FileImage(userToShow.profilePicture),
          fit: BoxFit.contain,
        ),
      );
    }
    else{
      boxDeco = new BoxDecoration(
        image: new DecorationImage(
          image: AssetImage('assets/images/defaultProfileImage.png'),
          fit: BoxFit.contain,
        ),
      );
    }

    return Scaffold(
        body: Center(
      child: Form(
        key: this._formKey,
        child: Padding(
          padding: const EdgeInsets.all(32.0),
          child: Column(
            mainAxisSize: MainAxisSize.min,
            children: <Widget>[
              new GestureDetector(
                  onTap: () {
                    Navigator.push(context,
                        MaterialPageRoute(builder: (context) => AboutUserScreen(user: userToShow,)));
                  },
                  child: Container(
                    constraints: new BoxConstraints.expand(
                      height: 300.0,
                    ),
                    padding: new EdgeInsets.only(left: 16.0, bottom: 8.0, right: 16.0),
                    decoration: boxDeco,
                  ),
              ),
              //Grab Image from Local Storage - will not work with URLs!
              Text(userToShow.name),
              Text(userToShow.age.toString()),
              Text(userToShow.gender.toString()),
              SizedBox(height: 20.0),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                children: <Widget>[
                  RaisedButton(
                    key: Key('button_like'),
                    child: Text('Like', style: TextStyle(color: GLOBALS.secondaryTextColor)),
                    onPressed: () => sendBehavior(true, userToShow.id),
                    color: GLOBALS.yadaMain,
                  ),
                  RaisedButton(
                    key: Key('button_dislike'),
                    child: Text('Dislike', style: TextStyle(color: GLOBALS.secondaryTextColor)),
                    onPressed: () => sendBehavior(false, userToShow.id),
                    color: GLOBALS.dangerButtonColor,
                  ),
                ],
              )
            ],
          ),
        ),
      ),
    ));
  }

  callTheAlgorithm() async {
    String url = GLOBALS.SERVERADDRESS + 'match/get';
    var response = await GLOBALS.httpClient.post(url, body: {
      'id': GLOBALS.currentUser.id
    });

    if (response.statusCode == 200) {
      handleMatcherResponse(response);
    } else {
      showError();
    }
  }

  handleMatcherResponse(http.Response response) async {
    List responseJson = json.decode(response.body);
    List<User> usersList = responseJson.map((m) => new User.fromJson(m)).toList();
    users = new Queue.from(usersList);

    await UserController.fillPictures(usersList);

    setState(() {
      isLoading = false;
    });

    displayNewUser();
  }

  displayNewUser() {
    if (users.length == 0) {
      setState(() {
        noMatches = true;
      });
    } else {
      setState(() {
        userToShow = users.removeFirst();
      });
    }
  }

  showError() {
    setState(() {
      isError = true;
      isLoading = false;
    });

    UserInterfaceController.showError(scaffoldContext, "Server error.");
  }

  sendBehavior(bool isLiked, String chosenUserId) async {
    String url = GLOBALS.SERVERADDRESS + 'behavior/create';

    var response = await GLOBALS.httpClient.post(url, body: {
      'userId': GLOBALS.currentUser.id,
      'chosenUserId': chosenUserId,
      'isLiked': isLiked.toString(),
    });

    if (response.statusCode == 200) {
      displayNewUser();
    } else {
      // If that response was not OK
      showError();
    }
  }

  void updatePermissionStatus(PermissionStatus status) {
    if(status != _status){
      setState((){
        _status = status;
        if(_status != PermissionStatus.granted){
          PermissionHandler().requestPermissions([PermissionGroup.locationWhenInUse])
              .then(checkUpdatedPermissionStatus);
        }
        else{
          callTheAlgorithm();
        }
      });
    }
  }

  void checkUpdatedPermissionStatus(Map<PermissionGroup, PermissionStatus> statuses){
    final status = statuses[PermissionGroup.locationWhenInUse];
    if(status != PermissionStatus.granted){
      setState((){
        isLoading = false;
        gpsDenied = true;
      });
    }
    else{
      setState((){
        gpsDenied = false;
      });
      callTheAlgorithm();
    }
  }
}
