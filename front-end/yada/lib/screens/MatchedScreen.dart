import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:yada/Controller/UserController.dart';
import 'package:yada/GLOBALS.dart';
import 'package:yada/Modals/MatchedUser.dart';
import 'package:yada/screens/ChatScreen.dart';


class MatchedScreen extends StatefulWidget {
  @override
  MatchedScreenState createState() => new MatchedScreenState();
}

class MatchedScreenState extends State<MatchedScreen> {

  final double pictureSize = 100.0;
  bool isError = false;

  @override
  void initState() {
    super.initState();
    if (!GLOBALS.matchedLoading) {
      checkNewMatches();
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        automaticallyImplyLeading: false,
        title: Text("Match"),
       leading: IconButton(icon: Icon(Icons.favorite), onPressed: (){
         Navigator.pop(context);
       },
       key: Key('button_backMatched'),),
      ),

      body: buildView(),
    );
  }

  void checkNewMatches() async {
    if (await UserController.getMatchedUsers()) {
      setState(() {
        null;
      });
    }
  }

  Widget buildView() {
    if(isError) {
      return Center(
        child: Text(
            "An error occurred..."
        ),
      );
    }

    if(GLOBALS.matchedLoading) {
      return Center(
       child: CircularProgressIndicator(valueColor: AlwaysStoppedAnimation<Color>(GLOBALS.yadaMain),),
      );
    }

    return ListView(
      children: buildChatContainer(),
    );
  }

  /// Builds the Match container listed at the top of the screen, like the Tinder matched persons.
  List<InkWell> buildMatchContainer() {

    List<InkWell> containers = new List<InkWell>();

    GLOBALS.matchedUser.forEach((mUser) {
      if (mUser.messages == null || mUser.messages.length == 0) {
        ImageProvider imageProvider;

        if (mUser.profilePicture != null) {
          imageProvider = FileImage(mUser.profilePicture);
        } else {
          imageProvider = AssetImage('assets/images/defaultProfileImage.png');
        }

        containers.add(new InkWell(
          child: Container(
            width: pictureSize,
            height: pictureSize,
            decoration: new BoxDecoration(
              shape: BoxShape.circle,
              color: Colors.green.shade400,
              image: new DecorationImage(
                image: imageProvider,
                fit: BoxFit.cover,
              ),
            ),
          ),
          onTap: () => showChat(mUser),
          borderRadius: BorderRadius.all(
              Radius.circular(50.0)),
          splashColor: Colors.teal.shade300,
        ));
      }
    });

    return containers;
  }

  /// Builds the overall view containing the MatchContainer and the already existing chat cards
  List<Center> buildChatContainer() {

    List<Center> containers = new List<Center>();

    if (GLOBALS.matchedUser != null && GLOBALS.matchedUser.length > 0) {

      bool onlyMatched = false;

      for (int i = 0; i < GLOBALS.matchedUser.length; i++) {
        if (GLOBALS.matchedUser[i].messages.length == 0) {
          onlyMatched = true;
        }
      }

      if (onlyMatched) {
        containers.add(new Center(
          child: Container(
              height: pictureSize + 10,
              child: Padding(
                padding: EdgeInsets.all(10),
                child: ListView(
                  scrollDirection: Axis.horizontal,
                  children: buildMatchContainer(),
                ),
              )
          ),
        ));
      }

      GLOBALS.matchedUser.forEach((mUser) {
        if (mUser.messages != null && mUser.messages.length > 0) {

          ImageProvider imageProvider;

          if (mUser.profilePicture != null) {
            imageProvider = FileImage(mUser.profilePicture);
          } else {
            imageProvider = AssetImage('assets/images/defaultProfileImage.png');
          }

          //Add the MatchedPersons Container
          containers.add(new Center(
              child: InkWell(
                child: Card(
                  child: Column(
                    mainAxisSize: MainAxisSize.max,
                    children: <Widget>[
                      ListTile(
                        leading: Container(
                          width: 56,
                          height: 56,
                          decoration: new BoxDecoration(
                              shape: BoxShape.circle,
                              color: Colors.red,
                              image: new DecorationImage(
                                image: imageProvider,
                                fit: BoxFit.fill,
                              ),
                          ),
                        ),
                        title: Text(mUser.name),
                        subtitle: Text(mUser.messages[mUser.messages.length - 1].message),
                        isThreeLine: false,
                      ),
                    ],
                  ),
                ),
                onTap: () => showChat(mUser),
                splashColor: Colors.grey.shade300,
              )
          ));
        }
      });
    }

    if (containers.length == 0) {
      containers.add(new Center(
        child: new Text(
          "Nothing here, yet...",
          textAlign: TextAlign.center,
        ),
      ));
    }

    return containers;
  }

  void showChat(MatchedUser chat) {
    Navigator.push(context, MaterialPageRoute(builder: (context) => ChatScreen(chat: chat)));
  }
}
