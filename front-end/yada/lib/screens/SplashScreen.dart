import 'package:flutter/material.dart';
import 'package:flutter_facebook_login/flutter_facebook_login.dart';
import 'package:yada/Controller/UserController.dart';
import 'package:yada/Controller/UserInterfaceController.dart';
import 'package:yada/GLOBALS.dart';
import 'package:yada/screens/LoginScreen.dart';

import 'BehaviorScreen.dart';
import 'UpdateUserScreen.dart';



class SplashScreen extends StatefulWidget {
  @override
  SplashScreenState createState() {
    return SplashScreenState();
  }
}

class SplashScreenState extends State<SplashScreen> {
  BuildContext scaffoldContext;

  @override
  void initState() {
    super.initState();
    checkFacebookLogin();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: Builder(
            builder: (scaffoldContext) {
              this.scaffoldContext = scaffoldContext;
              return Center(
                child: SingleChildScrollView(
                  padding: const EdgeInsets.all(15.0),
                  child: Column(
                    children: <Widget>[
                      Container(
                        child: GLOBALS.logo,
                        width: 125,
                        height: 125,
                      ),
                      GLOBALS.spacer,
                      Text(
                        "Yet Another Dating App",
                        style: TextStyle(
                            color: GLOBALS.yadaMain,
                            fontSize: 30.0,
                            fontWeight: GLOBALS.subHeading,
                            fontFamily: 'Montserrat'),
                      ),
                      SizedBox(height: 50),
                      SizedBox(
                        width: double.infinity,
                        height: 50,
                        child: RaisedButton(
                          key: Key('button_loginById'),
                            color: GLOBALS.yadaMain,
                            onPressed: () {
                              Navigator.push(context,
                                  MaterialPageRoute(
                                      builder: (context) => LoginScreen()));
                            },
                            child: Text(
                                'Login with email',
                                style: TextStyle(color: GLOBALS
                                    .secondaryTextColor))),
                      ),
                      GLOBALS.spacer,
                      SizedBox(
                        width: double.infinity,
                        height: 50,
                        child: RaisedButton(
                            color: GLOBALS.facebook,
                            onPressed: () {
                              loginViaFacebook();
                            },
                            child: Text('Login with Facebook',
                                style: TextStyle(color: GLOBALS
                                    .secondaryTextColor))),
                      )
                    ],
                  ),
                ),
              );
            }
        )
    );
  }

  void checkFacebookLogin() async {
    final facebookLogin = FacebookLogin();
    if (await facebookLogin.isLoggedIn) {

      var token = await facebookLogin.currentAccessToken;
      var result = await UserController.facebookLogin(token.userId, token.token);

      if (result == LoginStatus.Existed) {
        UserController.getMatchedUsers();
        Navigator.push(
            context,
            MaterialPageRoute(
              builder: (context) => BehaviorScreen(),
            )
        );
      }
    }
  }

  void loginViaFacebook() async {
    final facebookLogin = FacebookLogin();
    await facebookLogin.logOut();
    final result = await facebookLogin.logInWithReadPermissions(
        ['email', 'user_likes', 'user_events', 'user_birthday', 'user_gender']);

    if (result.status == FacebookLoginStatus.loggedIn) {
      if (result.accessToken.permissions.contains("user_likes") &&
          result.accessToken.permissions.contains("user_events") &&
          result.accessToken.permissions.contains("email")
          && result.accessToken.permissions.contains("user_birthday") &&
          result.accessToken.permissions.contains("user_gender")) {
        String facebookUserID = result.accessToken.userId;
        String token = result.accessToken.token;

        LoginStatus loginStatus = await UserController.facebookLogin(
            facebookUserID, token);
        StatefulWidget nextScreen = BehaviorScreen();

        switch (loginStatus) {
          case LoginStatus.Created:
            nextScreen = UpdateForm(userCreated: true,);
            UserController.getMatchedUsers();
            Navigator.push(
                context,
                MaterialPageRoute(
                  builder: (context) => nextScreen,
                )
            );
            break;
          case LoginStatus.Error:
            UserInterfaceController.showError(
                scaffoldContext, "LogIn Error, please try again later.");
            break;
          default:
            UserController.getMatchedUsers();
            Navigator.push(
                context,
                MaterialPageRoute(
                  builder: (context) => nextScreen,
                )
            );
            break;
        }

        Navigator.push(
            context,
            MaterialPageRoute(
              builder: (context) => nextScreen,
            )
        );
      } else {
        UserInterfaceController.showError(
            scaffoldContext, "Not every required permission was allowed.");
      }
    } else if (result.status == FacebookLoginStatus.cancelledByUser) {
      UserInterfaceController.showError(
          scaffoldContext, "Facebook login cancelled.");
    } else if (result.status == FacebookLoginStatus.error) {
      UserInterfaceController.showError(
          scaffoldContext, "Facebook login error!");
    }
  }
}
