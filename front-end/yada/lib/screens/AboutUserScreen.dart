import 'package:flutter/material.dart';
import 'package:yada/Modals/User.dart';
import 'package:yada/screens/MatchedScreen.dart';
import 'package:yada/screens/MatchedUserGallery.dart';


class AboutUserScreen extends StatefulWidget {
  final User user;

  AboutUserScreen({Key key, @required this.user}) : super(key: key);

  @override
  AboutUserState createState() => new AboutUserState(user);
}

class AboutUserState extends State<AboutUserScreen> {
  User user;

   AboutUserState(this.user);

  @override
  Widget build(BuildContext context) {

    ImageProvider imageProvider;

    if (user.profilePicture != null) {
      imageProvider = FileImage(user.profilePicture);
    } else {
      imageProvider = AssetImage('assets/images/defaultProfileImage.png');
    }

    return Scaffold(
      appBar: AppBar(
        title: Text("User Profile"),
        actions: <Widget>[
        new IconButton(
        icon: new Icon(Icons.image),
        onPressed: () {
          Navigator.push(context,
              MaterialPageRoute(builder: (context) => MatchedUserGallery(user: user,)));
        },
      ),
          new IconButton(
            icon: new Icon(Icons.chat),
            onPressed: () {
              Navigator.push(context,
                  MaterialPageRoute(builder: (context) => MatchedScreen()));
            },
          )
        ],
      ),
      body: Container(
        margin: const EdgeInsets.only(top: 10.0),
        child: Stack(
          children: <Widget>[
            Positioned(
              child: Center(
                child: Column(children: <Widget>[
            Container(

            height: 300.0,
            child: Card(semanticContainer: true,
              clipBehavior: Clip.antiAliasWithSaveLayer,

              child: Image(
                image: imageProvider,
                fit: BoxFit.fill,
              ),

              elevation: 5,
              margin: EdgeInsets.all(10.0))
            ),

                  SizedBox(height: 15.0),
                  Text(
                      user.name +', ' + user.age.toString(),
                    style: TextStyle(
                        fontSize: 30.0,
                        fontWeight: FontWeight.bold,
                        fontFamily: 'Montserrat'),
                  ),
                  SizedBox(height: 15.0),
                  Align(
                    alignment: Alignment.center,
                   child: Column(children: <Widget>[

                    Text(

                      'Gender: ' + user.gender,

                       textAlign: TextAlign.justify,
                       style: TextStyle(
                       fontSize: 17.0,
                       fontWeight: FontWeight.w300,
                       fontFamily: 'Montserrat'),
                     ),
                     SizedBox(height: 10.0),
                     Text(
                       'Interested in: ' + user.genderInterest,
                       textAlign: TextAlign.justify,
                       style: TextStyle(
                           fontSize: 17.0,
                           fontWeight: FontWeight.w300,
                           fontFamily: 'Montserrat'),
                     )
                   ])
                  ),

                ]),
              ),
            )
          ],
        ),
      ),
    );
  }
}
