import 'dart:io';

import 'package:flutter/material.dart';
import 'package:image_picker/image_picker.dart';
import 'package:path/path.dart' as Path;
import 'package:path_provider/path_provider.dart';
import 'package:yada/Controller/UserController.dart';
import 'package:yada/GLOBALS.dart';

class UserPicturesScreen extends StatefulWidget {
  @override
  _UserPicturesScreenState createState() => new _UserPicturesScreenState();
}

class _UserPicturesScreenState extends State<UserPicturesScreen> {

  String imageUrl = '';
  bool isLoading = false;
  BuildContext scaffoldContext;

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return new Scaffold(
      appBar: AppBar(
        title: Text('Gallery'),
      ),
      body: Builder(
        builder: (scaffoldContext) {
          this.scaffoldContext = scaffoldContext;
          return Center(
            child: new ListView(
              children: getContainer(),
            ),
          );
        },
      ),
      floatingActionButton: FloatingActionButton(
        backgroundColor: GLOBALS.yadaMain,
        child: Icon(Icons.add),
        onPressed: onImageGet,
      ),
    );
  }

  List<Widget> getContainer() {
    List <Widget> containers = <Widget>[];

    if (GLOBALS.currentUser.pictures != null && GLOBALS.currentUser.pictures.length > 0) {
      for (File pic in GLOBALS.currentUser.pictures) {
        if (pic != null && pic.existsSync()) {
          IconButton iconButton;

          if (pic != GLOBALS.currentUser.profilePicture) {
            iconButton = new IconButton(
              icon: Icon(Icons.star_border),
              color: Colors.deepPurpleAccent,
              iconSize: 40,
              onPressed: () async {
                if (await UserController.updateProfilePicture(pic)) {
                  setState(() {
                    return true;
                  });
                }
              },
            );
          } else {
            iconButton = new IconButton(
              icon: Icon(Icons.star),
              color: Colors.deepPurpleAccent,
              iconSize: 40,
            );
          }

          Widget container = new Container(
              constraints: new BoxConstraints.expand(
                height: 200.0,
              ),
              padding: new EdgeInsets.only(left: 16.0, bottom: 8.0, right: 16.0),
              decoration: new BoxDecoration(
                image: new DecorationImage(
                  image: Image.file(pic).image,
                  fit: BoxFit.cover,
                ),
              ),
              child: new Stack(
                children: <Widget>[
                  new Positioned(
                    right: 0.0,
                    top: 16.0,
                    child: new IconButton(
                      icon: Icon(Icons.remove_circle),
                      color: Colors.red.shade900,
                      iconSize: 40,
                      onPressed: () async {
                        if (await UserController.deletePicture(pic)) {
                          setState(() {
                            return true;
                          });
                        }
                      },
                    ),
                  ),
                  new Positioned(
                    left: 0.0,
                    top: 16.0,
                    child: iconButton,
                  ),
                ],
              )
          );

          containers.add(container);
        }
      }
    }

    return containers;
  }

  Future<File> getImage() async{
    var image = await ImagePicker.pickImage(source: ImageSource.gallery);
    return image;
  }

  void onImageGet() async {
    var image = await getImage();
    if (image.path != null) {
      var appDirectory = await getApplicationDocumentsDirectory();
      var newImage = await image.copy(appDirectory.path + '/' + DateTime.now().millisecondsSinceEpoch.toString() + Path.extension(image.path));

      if (await UserController.addPicture(scaffoldContext, newImage)) {
        setState(() {
          GLOBALS.currentUser.pictures.add(newImage);
        });
      }
    }
  }
}
