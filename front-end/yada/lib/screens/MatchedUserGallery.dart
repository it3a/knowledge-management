import 'dart:io';
import 'package:flutter/material.dart';
import 'package:yada/Modals/User.dart';

class MatchedUserGallery extends StatefulWidget {
  final User user;

  MatchedUserGallery({Key key, @required this.user}) : super(key: key);

  @override
  MatchedUserGalleryState createState() => new MatchedUserGalleryState(user);
}

class MatchedUserGalleryState extends State<MatchedUserGallery> {
  User user;
  MatchedUserGalleryState(this.user);

  BuildContext scaffoldContext;

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return new Scaffold(
      appBar: AppBar(
        title: Text('Matched User Gallery'),
      ),
      body: Builder(
        builder: (scaffoldContext) {
          this.scaffoldContext = scaffoldContext;
          return Center(
            child: new PageView(
              children: getContainer(),
              //user.pictures
            ),
          );
        },
      ),
    );
  }

  List<Widget> getContainer() {
    List <Widget> containers = <Widget>[];

    if (user.pictures != null && user.pictures.length > 0) {
      for (File pic in user.pictures) {
        if (pic != null && pic.existsSync()) {

          Widget container = new Container(
              constraints: new BoxConstraints.expand(
                height: 200.0,
              ),
              padding: new EdgeInsets.only(left: 16.0, bottom: 8.0, right: 16.0),
              decoration: new BoxDecoration(
                image: new DecorationImage(
                  image: Image.file(pic).image,
                  fit: BoxFit.cover,
                ),
              ),
          );

          containers.add(container);
        }
      }
    }

    return containers;
  }
}