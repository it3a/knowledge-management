import 'package:flutter/material.dart';
import 'package:flutter_facebook_login/flutter_facebook_login.dart';
import 'package:yada/screens/BehaviorScreen.dart';
import 'package:yada/screens/SplashScreen.dart';
import 'package:yada/screens/UpdateUserScreen.dart';

import '../GLOBALS.dart';
import 'UserPicturesScreen.dart';

class UserProfileScreen extends StatefulWidget {
  @override
  UserProfileState createState() => new UserProfileState();
}

class UserProfileState extends State<UserProfileScreen> {
  @override
  Widget build(BuildContext context) {

    ImageProvider imageProvider;


    if (GLOBALS.currentUser == null) {
      return Scaffold(
          appBar: AppBar(
            title: Text("User Profile"),
            actions: <Widget>[
              new IconButton(
                icon: new Icon(Icons.favorite),
                onPressed: () {
                  Navigator.push(context,
                      MaterialPageRoute(builder: (context) => BehaviorScreen()));
                },
              )
            ],
          ),
          body: Container(

          )
      );
    }

    if (GLOBALS.currentUser.profilePicture != null) {
      imageProvider = FileImage(GLOBALS.currentUser.profilePicture);
    } else {
      imageProvider = AssetImage('assets/images/defaultProfileImage.png');
    }

    return Scaffold(
      appBar: AppBar(
        title: Text("User Profile"),
        actions: <Widget>[
          new IconButton(
            icon: new Icon(Icons.favorite),
            onPressed: () {
              Navigator.push(context,
                  MaterialPageRoute(builder: (context) => BehaviorScreen()));
            },
          )
        ],
      ),
      body: Container(
        margin: const EdgeInsets.only(top: 150.0),
        child: Stack(
          children: <Widget>[
            Positioned(
              child: Center(
                child: Column(children: <Widget>[
                  Container(
                      width: 150.0,
                      height: 150.0,
                      decoration: BoxDecoration(
                          color: GLOBALS.dangerButtonColor,
                          image: DecorationImage(
                              image: imageProvider,
                              fit: BoxFit.cover),
                          borderRadius: BorderRadius.all(
                              Radius.circular(100.0)),
                          boxShadow: [
                            BoxShadow(blurRadius: 3.0, color:GLOBALS.shadow)
                          ]),
                  ),
                  GLOBALS.spacer,
                  Text(
                    GLOBALS.currentUser.name + ', ' + GLOBALS.currentUser.age.toString(),
                    style: TextStyle(
                        fontSize: 30.0,
                        fontWeight: GLOBALS.heading,
                        fontFamily: 'Montserrat'),
                  ),
                  SizedBox(height: 15.0),
                  Align(
                    alignment: Alignment.center,
                    child: Container(
                      child: Text(
                        'Some guy that rocks and likes women..', // TODO: fetch from backend
                        textAlign: TextAlign.justify,
                        style: TextStyle(
                            fontSize: 17.0,
                            fontStyle: FontStyle.italic,
                            fontWeight: GLOBALS.description,
                            fontFamily: 'Montserrat'),
                      ),
                    ),
                  ),
                  ButtonTheme.bar(
                    child: ButtonBar(
                      alignment: MainAxisAlignment.center,
                      children: <Widget>[
                        FlatButton(
                            onPressed: () {
                              Navigator.push(context,
                                  MaterialPageRoute(
                                      builder: (context) => UserPicturesScreen()));
                            },
                            color: GLOBALS.lightButtonColor,
                            padding: EdgeInsets.all(10.0),
                            child: Row(
                              children: <Widget>[
                                Icon(
                                  Icons.insert_drive_file,
                                  color: Colors.white,
                                  size: 18.0,
                                ),
                                Text(
                                  "Pictures",
                                  style: TextStyle(
                                      fontSize: 15.0,
                                      fontWeight: GLOBALS.button,
                                      color: Colors.white,
                                      fontFamily: 'Montserrat'),
                                )
                              ],
                            )),
                        FlatButton(
                          key: Key('button_logout'),
                            onPressed: () {
                              // kill the user session
                              logOut();
                            },
                            color: GLOBALS.dangerButtonColor,
                            padding: EdgeInsets.all(10.0),
                            child: Row(
                              children: <Widget>[
                                Icon(
                                  Icons.exit_to_app,
                                  color: Colors.white,
                                  size: 18.0,
                                ),
                                Text(
                                  "Logout",
                                  style: TextStyle(
                                      fontSize: 15.0,
                                      fontWeight: GLOBALS.button,
                                      color: Colors.white,
                                      fontFamily: 'Montserrat'),
                                )
                              ],
                            )),
                        FlatButton(
                          key: Key('button_editProfile'),
                            onPressed: () {
                              Navigator.push(context,
                                  MaterialPageRoute(
                                      builder: (context) => UpdateForm(userCreated: false,)));
                            },
                            color: GLOBALS.yadaMain,
                            padding: EdgeInsets.all(10.0),
                            child: Row(
                              children: <Widget>[
                                Icon(
                                  Icons.edit,
                                  color: Colors.white,
                                  size: 18.0,
                                ),
                                Text(
                                  "Edit Profile",
                                  style: TextStyle(
                                      fontSize: 15.0,
                                      fontWeight: GLOBALS.button,
                                      color: Colors.white,
                                      fontFamily: 'Montserrat'),
                                )
                              ],
                            )),
                      ],
                    ),
                  )
                ]),
              ),
            )
          ],
        ),
      ),
    );
  }

  void logOut() {
    GLOBALS.currentUser = null;

    final facebookLogin = FacebookLogin();
    facebookLogin.logOut();

    Navigator.push(context,
        MaterialPageRoute(
            builder: (context) => SplashScreen()
        )
    );
  }
}
