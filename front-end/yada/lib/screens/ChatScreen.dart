
import 'dart:async';
import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:http/http.dart' as http;
import 'package:yada/Controller/UserController.dart';
import 'package:yada/GLOBALS.dart';
import 'package:yada/Modals/MatchedUser.dart';
import 'package:yada/Modals/Message.dart';



class ChatScreen extends StatefulWidget {

  final MatchedUser chat;

  ChatScreen({Key key, @required this.chat}) : super(key: key);

  @override
  ChatScreenState createState() => new ChatScreenState(chat);
}

class ChatScreenState extends State<ChatScreen> {

  final TextEditingController textEditingController = new TextEditingController();
  final ScrollController scrollController = new ScrollController();

  MatchedUser chat;
  bool isLoading = true;
  bool isError = false;
  Text appBarTitleText = new Text("Chat");
  Timer timer;

  ChatScreenState(this.chat);

  @override
  void initState() {
    super.initState();
    timer = Timer.periodic(Duration(seconds: 5), (Timer t) => loadMessages());
    isLoading = false;
  }

  @override
  void dispose() {
    timer?.cancel();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {

    return Scaffold(
      appBar: AppBar(
        automaticallyImplyLeading: false,
        title: appBarTitleText,
          leading: IconButton(icon: Icon(Icons.message), onPressed: (){
            Navigator.pop(context);
          },
          key: Key('button_backChat'),)
      ),
      body: buildView(),
    );
  }

  Widget buildView() {
    if(isError) {
      return Center(
        child: Text(
            "An error occurred..."
        ),
      );
    }

    if(isLoading) {
      return Center(
        child: Text(
            "Loading..."
        ),
      );
    }

    return Column(
      children: <Widget> [
        buildChatView(),
        buildInput(),
      ],
    );
  }

  Widget buildChatView() {
    setState(() {
      appBarTitleText = Text(chat.name);
    });

    String previousSenderId = "";
    List<Center> chatView = new List<Center>();

    if (chat != null && chat.messages.length > 0) {

      chat.messages.forEach((message) {
        MainAxisAlignment axisAlignment = MainAxisAlignment.start;
        Color messageColor = Colors.grey.shade200;
        EdgeInsets messageMargin = EdgeInsets.all(5);
        IconData messageSent = Icons.clear;
        Widget sentIcon = Center();

        if (message.senderId == GLOBALS.currentUser.id) {
          axisAlignment = MainAxisAlignment.end;
          messageColor = Colors.green.shade100;

          if (message.isSent) {
            messageSent = Icons.check;
          }

          sentIcon = Positioned(
            right: 0,
            bottom: -5,
            child: new Icon(
                messageSent,
                size: 20,
                color: Colors.grey.shade500
            ),
          );
        }

        if (message.senderId != previousSenderId) {
          messageMargin = EdgeInsets.fromLTRB(5, 20, 5, 5);
        }

        chatView.add(new Center(
            child: Row(
              mainAxisAlignment: axisAlignment,
              children: <Widget>[
                Stack(
                  children: <Widget>[
                    Container(
                      decoration: new BoxDecoration(
                        color: messageColor,
                        borderRadius: BorderRadius.all(Radius.circular(10.0)),
                      ),
                      padding: EdgeInsets.all(8),
                      margin: messageMargin,
                      child: Text(message.message),
                      width: MediaQuery.of(context).size.width * 0.75,
                    ),
                    sentIcon
                  ],
                )
              ],
            )
        ));

        previousSenderId = message.senderId;
      });
    }

    return Flexible(
      child: ListView(
        children: chatView,
        controller: scrollController,
      ),
    );
  }

  void loadMessages() async {
    timer.cancel();

    bool isNewMessage = await UserController.getChat(chat);

    if (isNewMessage) {
      setState(() {
        null;
      });
    }

    timer = Timer.periodic(Duration(seconds: 5), (Timer t) => loadMessages());
  }

  void onSendMessage(String content) async {

    Message localMessage = new Message(id: "LocalNotSent", senderId: GLOBALS.currentUser.id, receiverId: chat.id, message: content, isSent: false);
    setState(() {
      chat.messages.add(localMessage);
      textEditingController.clear();
    });

    String url = GLOBALS.SERVERADDRESS + 'messaging/send';
    var response = await GLOBALS.httpClient.post(url, body: {
      'senderId': GLOBALS.currentUser.id,
      'receiverId': chat.id,
      'message': content
    });

    if (response.statusCode == 200) {
      var responseJson = json.decode(response.body);
      Message message = new Message.fromJson(responseJson);
      setState(() {
        chat.messages.removeLast();
        chat.messages.add(message);
      });
    } else {
      setState(() {
        isError = true;
      });
    }

    jumpToLastPosition();
  }

  void jumpToLastPosition() {
    scrollController.jumpTo(scrollController.position.maxScrollExtent);
  }

  Widget buildInput() {
    return Container(
      child: Row(
        children: <Widget>[
          // Edit text
          Flexible(

            child: Container(

              child: TextField(
                style: TextStyle(
                    color: GLOBALS.primary,
                    fontSize: 15.0,
                ),

                key: Key('textField_chatInput'),
                controller: textEditingController,
                decoration: InputDecoration(
                  contentPadding: EdgeInsets.all(10),
                  hintText: 'Type your message...',
                    border: OutlineInputBorder(borderRadius: BorderRadius.all(
                        Radius.circular(100.0))),
                  hintStyle: TextStyle(
                      color: Colors.grey.shade500
                  ),
                ),
              ),
            ),
          ),

          // Button send message
          Material(
            child: new Container(
              margin: new EdgeInsets.symmetric(horizontal: 8.0),
              child: new IconButton(
                key: Key('button_sendMessage'),
                icon: new Icon(Icons.send),
                onPressed: () => onSendMessage(textEditingController.text),
                color: GLOBALS.yadaMain,
              ),
            ),
            color: Colors.white,
          ),
        ],
      ),
      width: double.infinity,
      height: 50.0,
      decoration: new BoxDecoration(
          border: new Border(
              top: new BorderSide(
                  color: Colors.grey.shade500,
                  width: 0.5
              )
          ),
          color: Colors.white
      ),
    );
  }
}