class Message {

  final String id; // documentId
  final String senderId;
  final String receiverId;
  final String message;
  final DateTime time;
  final bool isSent;


  Message({this.id, this.senderId, this.receiverId, this.message, this.time, this.isSent,});

  factory Message.fromJson(Map<String, dynamic> json) {
    return Message(
      id: json['id'],
      senderId: json['senderId'],
      receiverId: json['receiverId'],
      message: json['message'],
      time: DateTime.parse(json['time']['seconds'].toString()),
      isSent: true,
    );
  }
}
