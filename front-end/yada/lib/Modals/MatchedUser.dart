import 'dart:io';
import 'Message.dart';

class MatchedUser {
  final String id;
  final String name;
  final String gender;
  final String description;
  final int age;
  File profilePicture;
  List<File> pictures;
  List<Message> messages = new List();

  MatchedUser({this.id, this.name, this.gender, this.description, this.age,});

  factory MatchedUser.fromJson(Map<String, dynamic> json) {
    return MatchedUser(
      id: json['userId'],
      name: json['name'],
      gender: json['gender'],
      description: json['description'],
      age: json['age'],
    );
  }
}
