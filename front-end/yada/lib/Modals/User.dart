import 'dart:io';

import 'package:google_maps_flutter/google_maps_flutter.dart';

class User {
  final String id;
  final String name;
  final String gender;
  final String genderInterest;
  final int age;
  final int lookingFrom;
  final int lookingTo;
  final int fbId;
  final String fbToken;
  final String fbEmail;
  LatLng location;
  bool isLocationByGPS;
  int searchRadius;
  File profilePicture;
  List<File> pictures = new List();

  User({this.id, this.name, this.gender, this.genderInterest, this.age, this.lookingFrom, this.lookingTo, this.location, this.searchRadius, this.isLocationByGPS, this.fbId, this.fbToken, this.fbEmail});

  factory User.fromJson(Map<String, dynamic> json) {
    double lat = 0;
    double lng = 0;
    if (json['geoPoint'] != null) {
      if (json['geoPoint']['latitude'] != null) {
        lat = json['geoPoint']['latitude'];
        lng = json['geoPoint']['longitude'];
      }
    }
    return User(
      id: json['id'],
      name: json['name'],
      gender: json['gender'],
      genderInterest: json['genderInterest'],
      age: json['age'],
      lookingFrom: json['matchAge']['lookingFrom'],
      lookingTo: json['matchAge']['lookingTo'],
      location: LatLng(lat, lng),
      searchRadius:json['searchDistance'],
      isLocationByGPS: json['isLocationByGPS'],
      fbId: json['fbId'],
      fbToken: json['fbToken'],
      fbEmail: json['fbEmail']
    );
  }
}
