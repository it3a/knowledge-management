import 'package:firebase_storage/firebase_storage.dart';
import 'package:flutter/material.dart';
import 'package:yada/Modals/MatchedUser.dart';
import 'package:http/http.dart' as http;

import 'Modals/User.dart';

class GLOBALS {

  static const String SERVERADDRESS = "http://protonz.ddns.net:8642/"; //NEEDS TO INCLUDE A SLASH AT THE END
  static const String MAPSAPIKEY = "AIzaSyCvPEx1uK0KzXbvCXBNoLT3s4omIr3p_rA";

  static http.Client httpClient;
  static FirebaseStorage firebaseStorage;
  static User currentUser;
  static List<MatchedUser> matchedUser = new List();
  static bool matchedLoading = true;

  //COLORS
  static Color primary = Colors.teal.shade900;
  static Color primaryButtonColor = Colors.green;
  static Color dangerButtonColor = Colors.red;
  static Color lightButtonColor = Colors.black38;
  static Color secondaryTextColor = Colors.white;
  static Color shadow = Colors.black;
  static Color yadaMain = Colors.deepPurple;
  static Color facebook = Colors.indigo;


  //Spacers
  static SizedBox spacer = SizedBox (height: 16,);


  //Font
  static FontWeight heading = FontWeight.bold;
  static FontWeight button = FontWeight.w200;
  static FontWeight description = FontWeight.w300;
  static FontWeight subHeading = FontWeight.w400;

  //Yada Logo
  static Image logo = Image.asset('assets/images/iconInverted.png');

}
