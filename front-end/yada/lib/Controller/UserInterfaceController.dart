import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';

class UserInterfaceController {
  static void showError(BuildContext bContext, String message) {
    Scaffold.of(bContext).showSnackBar(
      SnackBar(
        content: Text(message),
      ),
    );
  }
}