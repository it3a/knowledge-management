import 'dart:convert';
import 'dart:io';

import 'package:firebase_storage/firebase_storage.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:geolocator/geolocator.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:http/http.dart' as http;
import 'package:network_to_file_image/network_to_file_image.dart';
import 'package:path/path.dart' as Path;
import 'package:path_provider/path_provider.dart' as PathProvider;
import 'package:yada/Controller/UserInterfaceController.dart';
import 'package:yada/GLOBALS.dart';
import 'package:yada/Modals/MatchedUser.dart';
import 'package:yada/Modals/Message.dart';
import 'package:yada/Modals/User.dart';

class UserController {
  static void updateLocation(BuildContext bContext, bool isUpdatedGPS) async {
    var response = await GLOBALS.httpClient.post(GLOBALS.SERVERADDRESS + 'user/update-location',
      body: {
        'userId': GLOBALS.currentUser.id,
        'lat': GLOBALS.currentUser.location.latitude.toString(),
        'lng': GLOBALS.currentUser.location.longitude.toString(),
        'isLocationByGPS': isUpdatedGPS.toString()
      });

    if (response.statusCode == 200) {
      GLOBALS.currentUser.isLocationByGPS = isUpdatedGPS;
    } else {
      UserInterfaceController.showError(bContext, "Error while updating your location.");
    }
  }

  static Future<void> getUserLocation() async {
    Position location = await Geolocator().getCurrentPosition(desiredAccuracy: LocationAccuracy.high);

    try {
      GLOBALS.currentUser.location = LatLng(location.latitude, location.longitude);
    } on Exception {
      //TODO: Error handling
    }
  }

  static Future<bool> getMatchedUsers() async {
    String url = GLOBALS.SERVERADDRESS + 'behavior/get-matches';
    var response = await GLOBALS.httpClient.post(url, body: {
      'userId': GLOBALS.currentUser.id
    });

    if (response.statusCode == 200) {
      List responseJson = json.decode(response.body);
      List<MatchedUser> newMatchedList = responseJson.map((m) => new MatchedUser.fromJson(m)).toList();

      if (GLOBALS.matchedUser.length != newMatchedList.length) {
        GLOBALS.matchedUser = newMatchedList;
        await UserController.fillPictures(GLOBALS.matchedUser);
        await getExistingHistory();

        return true;
      }
      else {
        GLOBALS.matchedLoading = false;
        return false;
      }
    }
    else {
      GLOBALS.matchedLoading = true;
      return false;
    }
  }

  static Future getExistingHistory() async {
    if (GLOBALS.matchedUser != null) {
      for (int i = 0; i < GLOBALS.matchedUser.length; i++) {
        await getChat(GLOBALS.matchedUser[i]);
      }
    }

    GLOBALS.matchedLoading = false;
  }

  static Future<bool> getChat(MatchedUser mUser) async {
    String url = GLOBALS.SERVERADDRESS + 'messaging/get-chat';

    var response = await GLOBALS.httpClient.post(url, body: {
      'user1': GLOBALS.currentUser.id,
      'user2': mUser.id
    });

    if (response.statusCode == 200) {
      List responseJson = json.decode(response.body);
      List<Message> messages = responseJson.map((m) => new Message.fromJson(m)).toList();

      if (mUser.messages.length != messages.length) {
        mUser.messages = messages;
        return true;
      }
      else {
        return false;
      }
    }
    else {
      return false;
    }
  }

  static Future<LoginStatus> facebookLogin(String facebookID, String token) async {
    var response = await GLOBALS.httpClient.post(GLOBALS.SERVERADDRESS + 'user/authenticate',
        body: {
          'fbId': facebookID,
          'fbToken': token
        });

    if (response.statusCode == 200) {
      User user =  User.fromJson(json.decode(response.body));
      GLOBALS.currentUser = user;
      UserController.fillPictures(null);

      if (user.genderInterest != null) {
        return LoginStatus.Existed;
      } else {
        return LoginStatus.Created;
      }
    } else {
      return LoginStatus.Error;
    }
  }

  static Future<bool> updateProfilePicture(File img) async {
    var fileName = Path.basename(img.path);

    var response = await GLOBALS.httpClient.post(GLOBALS.SERVERADDRESS + 'user/make-profile-picture',
        body: {
          'userId': GLOBALS.currentUser.id,
          'imgPath': fileName
        });

    if (response.statusCode == 200) {
      GLOBALS.currentUser.profilePicture = img;
      return true;
    } else {
      //UserInterfaceController.showError(bContext, "Error while uploading your picture.");
      return false;
    }
  }

  static Future<bool> deletePicture(File img) async {
    var fileName = Path.basename(img.path);

    final StorageReference storageRef = GLOBALS.firebaseStorage.ref().child(GLOBALS.currentUser.id).child(fileName);
    storageRef.delete();

    var response = await GLOBALS.httpClient.post(GLOBALS.SERVERADDRESS + 'user/delete-picture',
        body: {
          'userId': GLOBALS.currentUser.id,
          'imgPath': fileName
        });

    if (response.statusCode == 200) {
      GLOBALS.currentUser.pictures.remove(img);
      return true;
    } else {
      //UserInterfaceController.showError(bContext, "Error while uploading your picture.");
      return false;
    }
  }

  static Future<bool> addPicture(BuildContext bContext, File file) async {
    UserController userController = new UserController();
    String fileName = await userController.uploadPicture(file);

    var response = await GLOBALS.httpClient.post(GLOBALS.SERVERADDRESS + 'user/upload-picture',
        body: {
          'userId': GLOBALS.currentUser.id,
          'imgPath': fileName
    });

    if (response.statusCode == 200) {
      return true;
    } else {
      UserInterfaceController.showError(bContext, "Error while uploading your picture.");
      return false;
    }
  }

  static Future<bool> fillPictures(List<Object> users) async {
    UserController userController = new UserController();

    if (users is List<User>) {
      for (User user in users) {
        user.profilePicture = await userController.getProfilePicture(user.id);
        List<File> pictures = await userController.getAllPictures(user.id);
        if (pictures != null) {
          user.pictures = pictures;
        }
      }
    }
    else if (users is List<MatchedUser>) {
      for (MatchedUser mUser in users) {
        mUser.profilePicture = await userController.getProfilePicture(mUser.id);
        List<File> pictures = await userController.getAllPictures(mUser.id);
        if (pictures != null) {
          mUser.pictures = pictures;
        }
      }
    }
    else {
      GLOBALS.currentUser.profilePicture = await userController.getProfilePicture(GLOBALS.currentUser.id);
      List<File> pictures = await userController.getAllPictures(GLOBALS.currentUser.id);
      if (pictures != null) {
        GLOBALS.currentUser.pictures = pictures;
      }
    }

    return true;
  }

  Future<File> getProfilePicture(String id) async {
    UserController userController = new UserController();

    var response = await GLOBALS.httpClient.post(GLOBALS.SERVERADDRESS + 'user/get-profile-picture',
        body: {
          'userId': id
        });

    if (response.statusCode == 200) {
      String profilePicName = response.body;
      File profileFile = await userController.getPicture(id, profilePicName);
      return profileFile;
    }
    else {
      return null;
    }
  }

  Future<List<File>> getAllPictures(String id) async {
    UserController userController = new UserController();
    List<File> files = new List<File>();

    var response = await GLOBALS.httpClient.post(GLOBALS.SERVERADDRESS + 'user/get-pictures',
        body: {
          'userId': id
        });

    if (response.statusCode == 200) {
      List<String> pictureNames = (json.decode(response.body) as List<dynamic>).cast<String>();
      for (String picName in pictureNames) {
        files.add(await userController.getPicture(id, picName));
      }
    }

    return files;
  }

  Future<File> getPicture(String id, String fileName) async {
    Directory dir = await PathProvider.getTemporaryDirectory();
    dir = new Directory(dir.path + '/' + id);
    dir.createSync(recursive: true);

    File file = new File(dir.path + '/' + fileName);
    try {
      final StorageReference ref = GLOBALS.firebaseStorage.ref().child(id).child(fileName);
      String url = await ref.getDownloadURL();

      var networkFile = NetworkToFileImage(url: url, file: file);

      var response = await http.get(url);

      networkFile.saveImageToTheLocalFile(response.bodyBytes);
      file.createSync(recursive: true);

      if (file.existsSync()) {
        return file;
      } else {
        return null;
      }
    }
    catch (ex) {
      return null;
    }
  }

  Future<String> uploadPicture(File file) async {

    final String fileName = DateTime.now().millisecondsSinceEpoch.toString() + Path.extension(file.path);
    final StorageReference storageRef = GLOBALS.firebaseStorage.ref().child(GLOBALS.currentUser.id).child(fileName);

    StorageUploadTask uploadTask = storageRef.putFile(file);
    await (await uploadTask.onComplete).ref.getDownloadURL();

    return fileName;
  }
}

enum LoginStatus {
  Error,
  Existed,
  Created
}